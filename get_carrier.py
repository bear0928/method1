import os
import pandas as pd


folder_path = 'test_order'
output_folder = 'test_carrier'

csv_files = [file for file in os.listdir(folder_path) if file.endswith('.csv')]

selected_zones = ["中山區", "松山區", "大安區", "信義區",
                  "南港區", "新店區", "文山區", "汐止區", "內湖特區"]

for csv_file in csv_files:

    file_path = os.path.join(folder_path, csv_file)
    df = pd.read_csv(file_path)

    # 將 '收件車手' 改為 'id'，'收件區域' 改為 'zone'
    df.rename(columns={'收件車手': 'id', '收件區域': 'zone'}, inplace=True)

    # 創建 'lng', 'lat', 'salary', 'speed', 'avtime' 欄位，並將其值設為空

    unique_combinations = df[['id', 'zone']].drop_duplicates()

    unique_combinations['lng'] = '121.5651079'
    unique_combinations['lat'] = '25.0502102'
    unique_combinations['salary'] = '0'
    unique_combinations['speed'] = '0.25'
    unique_combinations['avtime'] = '8:00:00'
    unique_combinations = unique_combinations.sort_values(by='id')
    unique_combinations = unique_combinations[unique_combinations['zone'].isin(
        selected_zones)]

    new_file_name = os.path.splitext(csv_file)[0] + '_test_carrier.csv'

    output_path = os.path.join(output_folder, new_file_name)
    unique_combinations.to_csv(output_path, index=False)

    print(f"已创建文件: {new_file_name}，存储在 {output_folder}")
