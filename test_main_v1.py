from packages.dispatch import dispatch
from packages.timetable import timetable
from packages.data_preprocess import address_preprocess
import pandas as pd
from datetime import datetime, timedelta, time

# read orders
order_df = pd.read_csv("test_order.csv", na_filter=False)
preprocess = address_preprocess()
# order_df = address_preprocess.get_complete_address(order_df)
selected_zones = ["中山區", "松山區", "大安區", "信義區",
                  "南港區", "新店區", "文山區", "汐止區", "內湖特區"]
excluded_orders = order_df[~order_df["收件區域"].isin(selected_zones)]
order_df = preprocess.process_csv(order_df, selected_zones)

# read carriers
carrier_df = pd.read_csv("test_carrier.csv")
carrier_df["avtime"] = pd.to_datetime(carrier_df["avtime"]).dt.time
carrier_df = carrier_df[carrier_df['zone'].isin(selected_zones)]

# input: peak_offpeak_hot_cold
peak_off_peak = [
    "peak",
    "off_peak",
    "off_peak",
    "off_peak",
    "off_peak",
    "off_peak",
    "off_peak",
]
peak_off_peak_dict = {index: value for index,
                      value in enumerate(peak_off_peak)}
zone_type = {
    "中山區": "hot",
    "信義區": "hot",
    "松山區": "hot",
    "南港區": "cold",
    "文山區": "cold",
    "新店區": "cold",
    "汐止區": "cold",
    "大安區": "normal",
    "內湖特區": "normal",
}


# parameters of timeslice
end_of_timeslot = [
    pd.to_datetime("10:20:00").time(),
    pd.to_datetime("11:40:00").time(),
    pd.to_datetime("13:00:00").time(),
    pd.to_datetime("14:40:00").time(),
    pd.to_datetime("16:00:00").time(),
    pd.to_datetime("17:20:00").time(),
]
end_of_timeslot_datetime = [
    pd.Timestamp.combine(pd.Timestamp.today(), time) for time in end_of_timeslot
]

timelimit_of_timeslot = [
    time(10, 10, 0),
    time(11, 30, 0),
    time(12, 50, 0),
    time(14, 30, 0),
    time(15, 50, 0),
    time(17, 10, 0),
]
time_limit = [
    pd.Timestamp.combine(pd.Timestamp.today(), t) for t in timelimit_of_timeslot
]

# init of a loop
current_time = pd.to_datetime("08:00:00").time()
current_datetime = datetime.combine(datetime.today(), current_time)
i = 0
total_pak = 0
assigned_pak = 0
sup_pak = 0


##
# assign order_slice to carrier
# assign unassigned_order to cold zone carriers
# update orders and carriers
##

while i < len(end_of_timeslot) and current_datetime <= end_of_timeslot_datetime[0]:
    # carrier_df['avtime'] = pd.to_datetime(carrier_df['avtime']).dt.time
    carrier_df.loc[carrier_df["avtime"] <
                   current_time, "avtime"] = current_time
    selected_orders = order_df[
        (order_df["叫件時間"] <= current_datetime) & (
            order_df["status"] == "unfinished")
    ]
    # order_df.loc[selected_orders.index, 'status'] = 'packaged'
    # print(current_datetime)
    # print(len(selected_orders))
    dis = dispatch()
    packages_df = dis.packing(selected_orders)
    # packages_df = dis.dont_packing(selected_orders)
    total_pak += len(packages_df)
    # print(len(packages_df))

    # 分配
    carrier_df, packages_df = dis.assign_paks_to_couriers(
        packages_df, carrier_df, timelimit_of_timeslot[i]
    )
    assigned_pak += len(packages_df[packages_df["status"] == "assigned"])
    # print(f"assigned_pak : {len(packages_df[packages_df['status'] == 'assigned'])}")

    # 修正已被完成的訂單狀態
    assigned_orders = packages_df[packages_df["status"] == "assigned"][
        ["id", "carrier_id"]
    ]
    for index, row in assigned_orders.iterrows():
        order_df.loc[order_df["id"].isin(row["id"]), ["status", "收件車手"]] = [
            "assigned",
            row["carrier_id"],
        ]

    # ################## TODO:不打包
    # assigned_ids = packages_df[packages_df['status'] == 'assigned']['id']
    # order_df.loc[order_df['id'].isin(assigned_ids), 'status'] = 'assigned'
    # ##################

    # TODO:在order_df中加上車手

    # 支援
    if i in peak_off_peak_dict and peak_off_peak_dict[i] == "peak":
        # 找出冷區車手
        cold_zone_list = [key for key,
                          value in zone_type.items() if value == "cold"]
        cold_zone_carrier_df = carrier_df[carrier_df["zone"].isin(
            cold_zone_list)]
        cold_zone_carrier_df = cold_zone_carrier_df.sort_values(
            by="salary", ascending=True
        )
        # print(cold_zone_carrier_df)
        supportable_packages = packages_df[
            (packages_df["status"] == "unassign")
            & (packages_df["pickup_zone"].map(zone_type) == "hot")
        ]  # FIXME: 可以設為熱區、非冷區等等
        # print(supportable_packages)
        # print(cold_zone_carrier_df)
        cold_zone_carrier_df, sup_packages = dis.assign_paks_to_couriers(
            supportable_packages,
            cold_zone_carrier_df,
            timelimit_of_timeslot[i],
            func="support",
        )
        sup_pak += len(sup_packages[sup_packages["status"] == "assigned"])
        # print(f"sup_pak : {len(sup_packages[sup_packages['status'] == 'assigned'])}")

        # 修改訂單狀態與車手的avalible_time
        sup_orders = sup_packages[sup_packages["status"] == "assigned"][
            ["id", "carrier_id"]
        ]
        for index, row in sup_orders.iterrows():
            order_df.loc[order_df["id"].isin(row["id"]), ["status", "收件車手"]] = [
                "sup",
                row["carrier_id"],
            ]
        carrier_df.update(cold_zone_carrier_df)

        # print(supportable_packages)

    # current_time的改變
    interval = timedelta(minutes=5)  # FIXME: 可以修改時間間隔
    if current_datetime == end_of_timeslot_datetime[0]:
        current_datetime = (current_datetime + interval)
    elif current_datetime + interval > end_of_timeslot_datetime[0]:
        current_datetime = end_of_timeslot_datetime[0]
    else:
        current_datetime = current_datetime + interval
    # if current_datetime == end_of_timeslot_datetime[i]:
    #     i += 1
    #     print("-------------")

    # Check if carrier_df['avtime'] is less than current_datetime
    mask = carrier_df['avtime'] < current_datetime.time()

    # If the condition is met, update carrier_df['avtime']
    carrier_df.loc[mask, 'avtime'] = current_datetime.time()

    # print(current_datetime)


# result
non_assigned_orders = order_df[order_df["status"] == "unfinished"]
non_assigned_counts = (
    non_assigned_orders[non_assigned_orders["status"] == "unfinished"]
    .groupby("pickup_zone")
    .size()
)
print(non_assigned_counts)
unfinished_count = len(order_df[order_df["status"] == "unfinished"])
print(f"未完成的訂單數量為: {unfinished_count}")

print(f"總訂單組合數量: {total_pak}")
print(f"被指派訂單組合數量:{assigned_pak}")
print(f"被支援訂單組合數量:{sup_pak}")

# 先將 carrier['salary'] 乘以 0.4
adjusted_rate = 0.4
carrier_df["salary"] = carrier_df["salary"] * adjusted_rate

# 計算標準差、中位數、平均數、最大值和最小值
std_deviation = carrier_df["salary"].std()
median = carrier_df["salary"].median()
mean = carrier_df["salary"].mean()
max_value = carrier_df["salary"].max()
min_value = carrier_df["salary"].min()

# 印出結果
print(f"標準差: {std_deviation}")
print(f"中位數: {median}")
print(f"平均數: {mean}")
print(f"最大值: {max_value}")
print(f"最小值: {min_value}")


# save result
carrier_df.to_csv("_carrier_.csv", index=False, encoding="utf-16")
non_assigned_orders.to_csv(
    "_non_assigned_order_.csv", index=False, encoding="utf-16"
)
order_df.to_csv("_order_.csv", index=False, encoding="utf-16")


# # draw
# import seaborn as sns
# import matplotlib.pyplot as plt

# sns.boxplot(x=carrier_df['salary'])
# plt.show()
