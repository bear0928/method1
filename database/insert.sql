USE express_system;
INSERT INTO `hub` (
        `name`,
        `t1_departure_time`,
        `t2_departure_time`,
        `t3_departure_time`,
        `t4_departure_time`,
        `t5_departure_time`,
        `t6_departure_time`,
        `t7_departure_time`
    )
VALUES (
        '八德站',
        '08:50:00',
        '10:20:00',
        '11:40:00',
        '13:00:00',
        '14:40:00',
        '16:00:00',
        '17:20:00'
    ),
    (
        '長安站',
        '08:50:00',
        '10:20:00',
        '11:40:00',
        '13:00:00',
        '14:40:00',
        '16:00:00',
        '17:20:00'
    );
-- FIXME: add zones of 長安hub
INSERT INTO `zone` (`name`, `hub`)
VALUES ('中山一區', '八德站'),
    ('松山區', '八德站'),
    ('大安區', '八德站'),
    ('信義區', '八德站'),
    ('南港區', '八德站'),
    ('新店區', '八德站'),
    ('文山區', '八德站'),
    ('汐止區', '八德站'),
    ('內湖特區', '八德站');
INSERT INTO `courier` (
        `id`,
        `zone`,
        `is_working`,
        `salary_accumulated`,
        `support_zone`
    )
VALUES (13, '松山區', 0, 0, NULL),
    (32, '松山區', 0, 0, NULL),
    (56, '松山區', 0, 0, NULL),
    (57, '松山區', 0, 0, NULL),
    (60, '松山區', 0, 0, NULL),
    (90, '松山區', 0, 0, NULL),
    (251, '松山區', 0, 0, NULL),
    (367, '松山區', 0, 0, NULL),
    (505, '松山區', 0, 0, NULL);
-- 設定為這些車手今日有上班
UPDATE `courier`
SET `is_working` = 1
WHERE `id` IN (13, 32, 56, 57, 60, 90, 251, 367, 505)
    AND `zone` = '松山區';
INSERT INTO `customer` (`id`, `title`, `address`)
VALUES ('28452047', '樂利數位科技股份有限公司', '台北市內湖區瑞湖街88號3樓之3'),
    ('53342456', '愛卡拉互動媒體股份有限公司', '台北市信義區東興路41號');
-- FIXME:之後改成可以用csv錄入
-- INSERT INTO `order` (
--         id,
--         `date`,
--         submission_time,
--         pickup_zone,
--         pickup_address,
--         pickup_lng,
--         pickup_lat,
--         `floor`,
--         pak_id,
--         fare,
--         delivery_zone,
--         delivery_address
--     )
-- VALUES (
--         '1234567890',
--         '2023-08-06',
--         '08:50:00',
--         'Zone A',
--         '123 Main Street',
--         '123.456',
--         '78.901',
--         '3rd Floor',
--         NULL,
--         '80',
--         'undefined',
--         'undefined'
--     );