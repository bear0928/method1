DROP DATABASE IF EXISTS express_system;
CREATE DATABASE express_system;
USE express_system;
CREATE TABLE `hub` (
  `name` VARCHAR(40) PRIMARY KEY,
  `lng` VARCHAR(100) DEFAULT 121.56510795456256,
  `lat` VARCHAR(100) DEFAULT 25.05021023163576,
  `t1_departure_time` TIME NOT NULL,
  `t1_peak_offpeak` ENUM('peak', 'off-peak', 'normal', 'undefined') DEFAULT 'undefined',
  `t2_departure_time` TIME NOT NULL,
  `t2_peak_offpeak` ENUM('peak', 'off-peak', 'normal', 'undefined') DEFAULT 'undefined',
  `t3_departure_time` TIME NOT NULL,
  `t3_peak_offpeak` ENUM('peak', 'off-peak', 'normal', 'undefined') DEFAULT 'undefined',
  `t4_departure_time` TIME NOT NULL,
  `t4_peak_offpeak` ENUM('peak', 'off-peak', 'normal', 'undefined') DEFAULT 'undefined',
  `t5_departure_time` TIME NOT NULL,
  `t5_peak_offpeak` ENUM('peak', 'off-peak', 'normal', 'undefined') DEFAULT 'undefined',
  `t6_departure_time` TIME NOT NULL,
  `t6_peak_offpeak` ENUM('peak', 'off-peak', 'normal', 'undefined') DEFAULT 'undefined',
  `t7_departure_time` TIME NOT NULL,
  `t7_peak_offpeak` ENUM('peak', 'off-peak', 'normal', 'undefined') DEFAULT 'undefined'
);
-- peak_offpeak 每日進行更新
CREATE TABLE `zone` (
  `name` VARCHAR(100) PRIMARY KEY,
  `hub` VARCHAR(40) NOT NULL,
  `t1_zone_type` ENUM('hot', 'cold', 'normal', 'undefined') DEFAULT 'undefined',
  `t2_zone_type` ENUM('hot', 'cold', 'normal', 'undefined') DEFAULT 'undefined',
  `t3_zone_type` ENUM('hot', 'cold', 'normal', 'undefined') DEFAULT 'undefined',
  `t4_zone_type` ENUM('hot', 'cold', 'normal', 'undefined') DEFAULT 'undefined',
  `t5_zone_type` ENUM('hot', 'cold', 'normal', 'undefined') DEFAULT 'undefined',
  `t6_zone_type` ENUM('hot', 'cold', 'normal', 'undefined') DEFAULT 'undefined',
  `t7_zone_type` ENUM('hot', 'cold', 'normal', 'undefined') DEFAULT 'undefined',
  FOREIGN KEY (`hub`) REFERENCES `hub`(`name`)
);
CREATE TABLE `courier` (
  `id` INT PRIMARY KEY,
  `zone` VARCHAR(100) NOT NULL,
  `is_working` BOOLEAN DEFAULT 0,
  `salary_accumulated` INT DEFAULT 0,
  `support_zone` VARCHAR(100),
  -- 只有冷區且空閑的車手可以支援其他區域  
  FOREIGN KEY (`zone`) REFERENCES `zone`(`name`),
  FOREIGN KEY (`support_zone`) REFERENCES `zone`(`name`)
);
CREATE TABLE `customer` (
  `id` CHAR(8) PRIMARY KEY,
  `title` VARCHAR(100) NOT NULL,
  `address` VARCHAR(400) NOT NULL,
  `location` POINT,
  `email` VARCHAR(100)
);
CREATE TABLE `pak` (
  `id` INT PRIMARY KEY AUTO_INCREMENT,
  `zone` VARCHAR(100),
  `pickup_lng` VARCHAR(100) NOT NULL,
  `pickup_lat` VARCHAR(100) NOT NULL,
  `packing_time` DATETIME NOT NULL,
  `due_time` DATETIME NOT NULL,
  `pickup_courier` INT,
  `assigned` BOOLEAN DEFAULT 0,
  `pickup_is_delayed` BOOLEAN DEFAULT 0,
  `number_of_floors` INT DEFAULT 1,
  `pay` INT DEFAULT 0,
  FOREIGN KEY (`pickup_courier`) REFERENCES `courier`(`id`)
);
CREATE TABLE `order` (
  `id` VARCHAR(11) PRIMARY KEY,
  `date` VARCHAR(10) NOT NULL,
  `submission_time` TIME NOT NULL,
  `pickup_zone` VARCHAR(100),
  `pickup_address` VARCHAR(400) NOT NULL,
  `pickup_lng` VARCHAR(100) NOT NULL,
  `pickup_lat` VARCHAR(100) NOT NULL,
  `floor` VARCHAR(20) NOT NULL,
  -- FK
  `pak_id` INT DEFAULT NULL,
  -- 此處default先暫定為80
  `fare` VARCHAR(20) DEFAULT '80',
  `cus_id` CHAR(8),
  -- delivery的部分只是純粹紀錄，本次專題沒有要使用到
  `delivery_zone` VARCHAR(100) NOT NULL DEFAULT 'undefined',
  `delivery_address` VARCHAR(400) NOT NULL DEFAULT 'undefined',
  -- FOREIGN KEY (`pak_id`) REFERENCES `pak`(`id`),
  FOREIGN KEY (`cus_id`) REFERENCES `customer`(`id`) -- FOREIGN KEY (`pickup_zone`) REFERENCES `zone`(`name`) -- FIXME: 外鍵處理
);
CREATE TABLE `courier_schedule` (
  `date` VARCHAR(10) NOT NULL,
  `time_slot` INT CHECK (
    `time_slot` BETWEEN 1 AND 7
  ),
  `courier_id` INT,
  `num_of_pak` INT,
  `pak_id` INT UNIQUE,
  `completion_time` DATETIME,
  PRIMARY KEY (`time_slot`, `courier_id`, `num_of_pak`),
  FOREIGN KEY (`pak_id`) REFERENCES `pak`(`id`)
);