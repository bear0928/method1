import os
import pandas as pd
import re
import requests
import json
from datetime import datetime, timedelta, time
# --------------------------------------------------


class address_preprocess:

    def __init__(self):
        self.allowed_zones = ["中山區", "松山區", "大安區", "信義區",
                              "南港區", "新店區", "文山區", "汐止區", "內湖特區"]
        self.mailingAddressList = []
        self.count = 0

    def read_specific_csv(folder_path, file_name):
        """
        Read a specific CSV file in the specified folder.

        Parameters:
        - folder_path (str): The path to the folder containing the CSV file.
        - file_name (str): The name of the CSV file (including '.csv') to be read.

        Returns:
        - pd.DataFrame: A DataFrame containing the data from the specified CSV file.
        """

        file_path = os.path.join(folder_path, file_name)

        # Specify index_col=None and skip_blank_lines=True
        df = pd.read_csv(file_path, index_col=None, skip_blank_lines=True)

        # Drop columns with all NaN values
        df = df.dropna(axis=1, how='all')

        # Add a new column 'source_file' with the source file name (without '.csv')
        df['date'] = os.path.splitext(file_name)[0]

        return df

    def process_df(order_df):
        selected_columns = ['案件序號', 'date', '叫件時間',
                            '收件車手', '收件區域', '收件地址', '合計運費']

        order_df = order_df[selected_columns]

        order_df['lat'] = ''
        order_df['lng'] = ''
        order_df['floor'] = ''
        order_df['address'] = ''
        order_df['收件車手'] = ''

        order_df['叫件時間'] = pd.to_datetime(order_df['叫件時間'])
        order_df['叫件時間'] = order_df['叫件時間'].dt.time
        order_df['status'] = "unfinished"
        order_df = order_df.rename(columns={'叫件時間': 'time'})
        order_df = order_df.rename(columns={'合計運費': 'fare'})
        order_df = order_df.rename(columns={'收件車手': 'carrier'})
        order_df = order_df.rename(columns={'收件區域': 'zone'})
        order_df = order_df.rename(columns={'收件地址': 'address_un'})
        order_df = order_df.rename(columns={'案件編號': 'id'})

        order_df['zone_type'] = order_df['zone'].map(zone_type)

        return order_df

    def get_complete_address(self, order_data):
        self.mailingAddressList = []
        self.count = 0
        order_data = self.filter_orders_by_zone(order_data)
        mailingRegion, mailingAddressUncomplete = self.extract_pickup_data(
            order_data)
        self.recursive(mailingAddressUncomplete, mailingRegion)
        self.fill_mailing_address(order_data)
        self.extract_floor(order_data)
        # self.add_coor(order_data)
        return order_data

    def filter_orders_by_zone(self, order_data):
        data = pd.DataFrame()
        filtered_data = order_data[order_data["zone"].isin(self.allowed_zones)]
        data = pd.concat([data, filtered_data], ignore_index=True)
        return data

    def extract_pickup_data(self, order_data):
        # 從order_data中提取收件區域和收件地址(不完整)欄位
        pickup_zone = order_data["zone"]
        pickup_address = order_data["address_un"]

        return pickup_zone, pickup_address

    def fill_mailing_address(self, order_data):
        # 確保self.mailingAddressList和order_data的行數相同
        if len(self.mailingAddressList) != len(order_data):
            raise ValueError("self.mailingAddressList和order_data的行數不相符")

        # 將self.mailingAddressList放入order_data的收件地址(補全)欄位
        order_data["address"] = self.mailingAddressList

    def extract_floor(self, order_data):
        # 使用正則表達式提取樓層信息
        addresses = order_data["address"]
        pattern = r"號(\d+[a-zA-Z]?)[樓Ff]"
        floors = []
        for address in addresses:
            match = re.search(pattern, address)
            if match:
                floors.append(match.group(1))
            else:
                floors.append("None")
        if len(floors) != len(order_data):
            raise ValueError("floors和order_data的行數不相符")
        order_data["floor"] = floors

    def add_coor(self, order_data):
        # 使用正則表達式提取樓層信息
        addresses = order_data["address"]
        lngs = []
        lats = []
        for address in addresses:
            coor = self.addrToCoordination(address)
            lngs.append(coor[0])
            lats.append(coor[1])
        order_data["lng"] = lngs
        order_data["lat"] = lats

    def addrToCoordination(self, address):
        url = (
            "https://maps.googleapis.com/maps/api/geocode/json?address="
            + address
            + "&key=AIzaSyB0myPoG4SqLtVbofN1l7HWscj7mNAd6cg"
        )
        res = requests.get(url)
        js = json.loads(res.text)
        result = js["results"][0]["geometry"]["location"]
        lat = result["lat"]
        lng = result["lng"]
        return list(map(lambda x: float(x), [lng, lat]))

    def regionAdded(self, mailingAddress, mailingRegion):
        # print(mailingRegion)
        if mailingAddress.find("號") == -1:
            if mailingAddress.find("新北市") != -1:
                # print(mailingAddress)
                self.mailingAddressList.append(mailingAddress + "號")
            elif mailingAddress.find("台北市") != -1:
                # print(mailingAddress)
                self.mailingAddressList.append(mailingAddress + "號")
            elif mailingAddress.find("臺北市") != -1:
                # print(mailingAddress)
                self.mailingAddressList.append(mailingAddress + "號")
            elif (
                "三重區" == mailingRegion[self.count - 1]
                or "土城區" == mailingRegion[self.count - 1]
                or "中和區" == mailingRegion[self.count - 1]
                or "永和區" == mailingRegion[self.count - 1]
                or "汐止區" == mailingRegion[self.count - 1]
                or "板橋區" == mailingRegion[self.count - 1]
                or "林口區" == mailingRegion[self.count - 1]
                or "新店區" == mailingRegion[self.count - 1]
                or "新莊區" == mailingRegion[self.count - 1]
                or "蘆洲區" == mailingRegion[self.count - 1]
                or "五股區" == mailingRegion[self.count - 1]
                or "樹林區" == mailingRegion[self.count - 1]
                or "淡水區" == mailingRegion[self.count - 1]
                or "重莊特區" == mailingRegion[self.count - 1]
                and mailingAddress.find("新北市") == -1
            ):
                # print("新北市" + mailingAddress)
                self.mailingAddressList.append("新北市" + mailingAddress + "號")
            elif (
                "三重區" == mailingRegion[self.count - 1]
                or "土城區" == mailingRegion[self.count - 1]
                or "中和區" == mailingRegion[self.count - 1]
                or "永和區" == mailingRegion[self.count - 1]
                or "汐止區" == mailingRegion[self.count - 1]
                or "板橋區" == mailingRegion[self.count - 1]
                or "林口區" == mailingRegion[self.count - 1]
                or "新店區" == mailingRegion[self.count - 1]
                or "新莊區" == mailingRegion[self.count - 1]
                or "蘆洲區" == mailingRegion[self.count - 1]
                or "五股區" == mailingRegion[self.count - 1]
                or "樹林區" == mailingRegion[self.count - 1]
                or "淡水區" == mailingRegion[self.count - 1]
                or "重莊特區" == mailingRegion[self.count - 1]
                and mailingAddress.find("新北市") != -1
            ):
                self.mailingAddressList.append(mailingAddress + "號")
            elif (
                "士林區" == mailingRegion[self.count - 1]
                or "大同區" == mailingRegion[self.count - 1]
                or "大安區" == mailingRegion[self.count - 1]
                or "中山一區" == mailingRegion[self.count - 1]
                or "中山二區" == mailingRegion[self.count - 1]
                or "中山區" == mailingRegion[self.count - 1]
                or "中正區" == mailingRegion[self.count - 1]
                or "內湖特區" == mailingRegion[self.count - 1]
                or "內湖區" == mailingRegion[self.count - 1]
                or "文山區" == mailingRegion[self.count - 1]
                or "北投區" == mailingRegion[self.count - 1]
                or "松山區" == mailingRegion[self.count - 1]
                or "信義區" == mailingRegion[self.count - 1]
                or "南港區" == mailingRegion[self.count - 1]
                or "萬華區" == mailingRegion[self.count - 1]
                or "天母區" == mailingRegion[self.count - 1]
                or "大直區" == mailingRegion[self.count - 1]
                or "舊莊區" == mailingRegion[self.count - 1]
                and mailingAddress.find("台北市") == -1
            ):
                # print("台北市" + mailingAddress)
                self.mailingAddressList.append("台北市" + mailingAddress + "號")
            elif (
                "士林區" == mailingRegion[self.count - 1]
                or "大同區" == mailingRegion[self.count - 1]
                or "大安區" == mailingRegion[self.count - 1]
                or "中山一區" == mailingRegion[self.count - 1]
                or "中山二區" == mailingRegion[self.count - 1]
                or "中山區" == mailingRegion[self.count - 1]
                or "中正區" == mailingRegion[self.count - 1]
                or "內湖特區" == mailingRegion[self.count - 1]
                or "內湖區" == mailingRegion[self.count - 1]
                or "文山區" == mailingRegion[self.count - 1]
                or "北投區" == mailingRegion[self.count - 1]
                or "松山區" == mailingRegion[self.count - 1]
                or "信義區" == mailingRegion[self.count - 1]
                or "南港區" == mailingRegion[self.count - 1]
                or "萬華區" == mailingRegion[self.count - 1]
                or "天母區" == mailingRegion[self.count - 1]
                or "大直區" == mailingRegion[self.count - 1]
                or "舊莊區" == mailingRegion[self.count - 1]
                and mailingAddress.find("台北市") != -1
            ):
                self.mailingAddressList.append(mailingAddress + "號")
            else:
                self.mailingAddressList.append(mailingAddress)
        else:
            if mailingAddress.find("新北市") != -1:
                # print(mailingAddress)
                self.mailingAddressList.append(mailingAddress)
            elif mailingAddress.find("台北市") != -1:
                # print(mailingAddress)
                self.mailingAddressList.append(mailingAddress)
            elif mailingAddress.find("臺北市") != -1:
                # print(mailingAddress)
                self.mailingAddressList.append(mailingAddress)
            elif (
                "三重區" == mailingRegion[self.count - 1]
                or "土城區" == mailingRegion[self.count - 1]
                or "中和區" == mailingRegion[self.count - 1]
                or "永和區" == mailingRegion[self.count - 1]
                or "汐止區" == mailingRegion[self.count - 1]
                or "板橋區" == mailingRegion[self.count - 1]
                or "林口區" == mailingRegion[self.count - 1]
                or "新店區" == mailingRegion[self.count - 1]
                or "新莊區" == mailingRegion[self.count - 1]
                or "蘆洲區" == mailingRegion[self.count - 1]
                or "五股區" == mailingRegion[self.count - 1]
                or "樹林區" == mailingRegion[self.count - 1]
                or "淡水區" == mailingRegion[self.count - 1]
                or "重莊特區" == mailingRegion[self.count - 1]
                and mailingAddress.find("新北市") == -1
            ):
                # print("新北市" + mailingAddress)
                self.mailingAddressList.append("新北市" + mailingAddress)
            elif (
                "三重區" == mailingRegion[self.count - 1]
                or "土城區" == mailingRegion[self.count - 1]
                or "中和區" == mailingRegion[self.count - 1]
                or "永和區" == mailingRegion[self.count - 1]
                or "汐止區" == mailingRegion[self.count - 1]
                or "板橋區" == mailingRegion[self.count - 1]
                or "林口區" == mailingRegion[self.count - 1]
                or "新店區" == mailingRegion[self.count - 1]
                or "新莊區" == mailingRegion[self.count - 1]
                or "蘆洲區" == mailingRegion[self.count - 1]
                or "五股區" == mailingRegion[self.count - 1]
                or "樹林區" == mailingRegion[self.count - 1]
                or "淡水區" == mailingRegion[self.count - 1]
                or "重莊特區" == mailingRegion[self.count - 1]
                and mailingAddress.find("新北市") != -1
            ):
                self.mailingAddressList.append(mailingAddress)
            elif (
                "士林區" == mailingRegion[self.count - 1]
                or "大同區" == mailingRegion[self.count - 1]
                or "大安區" == mailingRegion[self.count - 1]
                or "中山一區" == mailingRegion[self.count - 1]
                or "中山二區" == mailingRegion[self.count - 1]
                or "中山區" == mailingRegion[self.count - 1]
                or "中正區" == mailingRegion[self.count - 1]
                or "內湖特區" == mailingRegion[self.count - 1]
                or "內湖區" == mailingRegion[self.count - 1]
                or "文山區" == mailingRegion[self.count - 1]
                or "北投區" == mailingRegion[self.count - 1]
                or "松山區" == mailingRegion[self.count - 1]
                or "信義區" == mailingRegion[self.count - 1]
                or "南港區" == mailingRegion[self.count - 1]
                or "萬華區" == mailingRegion[self.count - 1]
                or "天母區" == mailingRegion[self.count - 1]
                or "大直區" == mailingRegion[self.count - 1]
                or "舊莊區" == mailingRegion[self.count - 1]
                and mailingAddress.find("台北市") == -1
            ):
                # print("台北市" + mailingAddress)
                self.mailingAddressList.append("台北市" + mailingAddress)
            elif (
                "士林區" == mailingRegion[self.count - 1]
                or "大同區" == mailingRegion[self.count - 1]
                or "大安區" == mailingRegion[self.count - 1]
                or "中山一區" == mailingRegion[self.count - 1]
                or "中山二區" == mailingRegion[self.count - 1]
                or "中山區" == mailingRegion[self.count - 1]
                or "中正區" == mailingRegion[self.count - 1]
                or "內湖特區" == mailingRegion[self.count - 1]
                or "內湖區" == mailingRegion[self.count - 1]
                or "文山區" == mailingRegion[self.count - 1]
                or "北投區" == mailingRegion[self.count - 1]
                or "松山區" == mailingRegion[self.count - 1]
                or "信義區" == mailingRegion[self.count - 1]
                or "南港區" == mailingRegion[self.count - 1]
                or "萬華區" == mailingRegion[self.count - 1]
                or "天母區" == mailingRegion[self.count - 1]
                or "大直區" == mailingRegion[self.count - 1]
                or "舊莊區" == mailingRegion[self.count - 1]
                and mailingAddress.find("台北市") != -1
            ):
                self.mailingAddressList.append(mailingAddress)
            else:
                self.mailingAddressList.append(mailingAddress)

    def recursive(self, mailingAddressUncomplete, mailingRegion):
        for mailingAddress in mailingAddressUncomplete:
            self.count += 1
            # print(self.count)
            # print(mailingRegion[self.count-1])
            index = mailingAddress.find("一")
            index_2 = mailingAddress.find("二")
            index_3 = mailingAddress.find("三")
            index_4 = mailingAddress.find("四")
            index_5 = mailingAddress.find("五")
            index_6 = mailingAddress.find("六")
            index_7 = mailingAddress.find("七")
            index_8 = mailingAddress.find("八")
            index_9 = mailingAddress.find("九")

            if index != -1:  # 地址已經有「一」
                if mailingAddress.find("段") != -1:  # 地址已經有寫段
                    self.regionAdded(mailingAddress, mailingRegion)
                elif mailingAddress.find("路") != -1:  # 地址已經有寫路
                    mailingAddress = (
                        mailingAddress[: index + 1] +
                        "段" + mailingAddress[index + 1:]
                    )
                    self.regionAdded(mailingAddress, mailingRegion)
                elif index == 0:  # 例外處理:一在地址最前面
                    self.regionAdded(mailingAddress, mailingRegion)
                else:  # 地址沒有寫段跟路
                    mailingAddress = (
                        mailingAddress[:index]
                        + "路"
                        + mailingAddress[index: index + 1]
                        + "段"
                        + mailingAddress[index + 1:]
                    )
                    self.regionAdded(mailingAddress, mailingRegion)

            elif index_2 != -1:  # 地址已經有「二」
                if mailingAddress.find("段") != -1:  # 地址已經有寫段
                    self.regionAdded(mailingAddress, mailingRegion)
                elif mailingAddress.find("路") != -1:  # 地址已經有寫路
                    mailingAddress = (
                        mailingAddress[: index_2 + 1]
                        + "段"
                        + mailingAddress[index_2 + 1:]
                    )
                    self.regionAdded(mailingAddress, mailingRegion)
                elif index_2 == 0:  # 例外處理:二在地址最前面
                    self.regionAdded(mailingAddress, mailingRegion)
                else:
                    mailingAddress = (
                        mailingAddress[:index_2]
                        + "路"
                        + mailingAddress[index_2: index_2 + 1]
                        + "段"
                        + mailingAddress[index_2 + 1:]
                    )
                    self.regionAdded(mailingAddress, mailingRegion)

            elif index_3 != -1:  # 地址已經有「三」
                if mailingAddress.find("段") != -1:  # 地址已經有寫段
                    self.regionAdded(mailingAddress, mailingRegion)
                elif mailingAddress.find("路") != -1:  # 地址已經有寫路
                    mailingAddress = (
                        mailingAddress[: index_3 + 1]
                        + "段"
                        + mailingAddress[index_3 + 1:]
                    )
                    self.regionAdded(mailingAddress, mailingRegion)
                elif index_3 == 0:  # 例外處理:三在地址最前面
                    self.regionAdded(mailingAddress, mailingRegion)
                else:
                    final_string = (
                        mailingAddress[:index_3]
                        + "路"
                        + mailingAddress[index_3: index_3 + 1]
                        + "段"
                        + mailingAddress[index_3 + 1:]
                    )
                    self.regionAdded(mailingAddress, mailingRegion)

            elif index_4 != -1:  # 地址已經有「四」
                if mailingAddress.find("段") != -1:  # 地址已經有寫段
                    self.regionAdded(mailingAddress, mailingRegion)
                elif mailingAddress.find("路") != -1:  # 地址已經有寫路
                    mailingAddress = (
                        mailingAddress[: index_4 + 1]
                        + "段"
                        + mailingAddress[index_4 + 1:]
                    )
                    self.regionAdded(mailingAddress, mailingRegion)
                elif index_4 == 0:  # 例外處理:四在地址最前面
                    self.regionAdded(mailingAddress, mailingRegion)
                else:
                    mailingAddress = (
                        mailingAddress[:index_4]
                        + "路"
                        + mailingAddress[index_4: index_4 + 1]
                        + "段"
                        + mailingAddress[index_4 + 1:]
                    )
                    self.regionAdded(mailingAddress, mailingRegion)

            elif index_5 != -1:  # 地址已經有「五」
                if mailingAddress.find("段") != -1:  # 地址已經有寫段
                    self.regionAdded(mailingAddress, mailingRegion)
                elif mailingAddress.find("路") != -1:  # 地址已經有寫路
                    mailingAddress = (
                        mailingAddress[: index_5 + 1]
                        + "段"
                        + mailingAddress[index_5 + 1:]
                    )
                    self.regionAdded(mailingAddress, mailingRegion)
                elif index_5 == 0:  # 例外處理:五在地址最前面
                    self.regionAdded(mailingAddress, mailingRegion)
                else:
                    mailingAddress = (
                        mailingAddress[:index_5]
                        + "路"
                        + mailingAddress[index_5: index_5 + 1]
                        + "段"
                        + mailingAddress[index_5 + 1:]
                    )
                    self.regionAdded(mailingAddress, mailingRegion)

            elif index_6 != -1:  # 地址已經有「六」
                if mailingAddress.find("段") != -1:  # 地址已經有寫段
                    self.regionAdded(mailingAddress, mailingRegion)
                elif mailingAddress.find("路") != -1:  # 地址已經有寫路
                    mailingAddress = (
                        mailingAddress[: index_6 + 1]
                        + "段"
                        + mailingAddress[index_6 + 1:]
                    )
                    self.regionAdded(mailingAddress, mailingRegion)
                elif index_6 == 0:  # 例外處理:六在地址最前面
                    self.regionAdded(mailingAddress, mailingRegion)
                else:
                    mailingAddress = (
                        mailingAddress[:index_6]
                        + "路"
                        + mailingAddress[index_6: index_6 + 1]
                        + "段"
                        + mailingAddress[index_6 + 1:]
                    )
                    self.regionAdded(mailingAddress, mailingRegion)

            elif index_7 != -1:  # 地址已經有「七」
                if mailingAddress.find("段") != -1:  # 地址已經有寫段
                    self.regionAdded(mailingAddress, mailingRegion)
                elif mailingAddress.find("路") != -1:  # 地址已經有寫路
                    mailingAddress = (
                        mailingAddress[: index_7 + 1]
                        + "段"
                        + mailingAddress[index_7 + 1:]
                    )
                    self.regionAdded(mailingAddress, mailingRegion)
                elif index_7 == 0:  # 例外處理:七在地址最前面
                    self.regionAdded(mailingAddress, mailingRegion)
                else:  # 地址沒有寫路跟段
                    mailingAddress = (
                        mailingAddress[:index_7]
                        + "路"
                        + mailingAddress[index_7: index_7 + 1]
                        + "段"
                        + mailingAddress[index_7 + 1:]
                    )
                    self.regionAdded(mailingAddress, mailingRegion)

            elif index_8 != -1:  # 地址已經有「八」
                if mailingAddress.find("段") != -1:  # 地址已經有寫段
                    self.regionAdded(mailingAddress, mailingRegion)
                elif mailingAddress.find("路") != -1:  # 地址已經有寫路
                    mailingAddress = (
                        mailingAddress[: index_8 + 1]
                        + "段"
                        + mailingAddress[index_8 + 1:]
                    )
                    self.regionAdded(mailingAddress, mailingRegion)
                elif index_8 == 0:  # 例外處理:八在地址最前面
                    self.regionAdded(mailingAddress, mailingRegion)
                else:
                    mailingAddress = (
                        mailingAddress[:index_8]
                        + "路"
                        + mailingAddress[index_8: index_8 + 1]
                        + "段"
                        + mailingAddress[index_8 + 1:]
                    )
                    self.regionAdded(mailingAddress, mailingRegion)

            elif index_9 != -1:  # 地址已經有「九」
                if mailingAddress.find("段") != -1:  # 地址已經有寫段
                    self.regionAdded(mailingAddress, mailingRegion)
                elif mailingAddress.find("路") != -1:  # 地址已經有寫路
                    mailingAddress = (
                        mailingAddress[: index_9 + 1]
                        + "段"
                        + mailingAddress[index_9 + 1:]
                    )
                    self.regionAdded(mailingAddress, mailingRegion)
                elif index_9 == 0:  # 例外處理:九在地址最前面
                    self.regionAdded(mailingAddress, mailingRegion)

                else:
                    mailingAddress = (
                        mailingAddress[:index_9]
                        + "路"
                        + mailingAddress[index_9: index_9 + 1]
                        + "段"
                        + mailingAddress[index_9 + 1:]
                    )
                    self.regionAdded(mailingAddress, mailingRegion)
            else:
                self.regionAdded(mailingAddress, mailingRegion)


# --------------------------------------------------
peak_off_peak = [
    "peak",
    "normal",
    "normal",
    "normal",
    "normal",
    "off_peak",
    "off_peak",
]
peak_off_peak_dict = {index: value for index,
                      value in enumerate(peak_off_peak)}
zone_type = {
    "中山區": "hot",
    "信義區": "normal",
    "松山區": "hot",
    "南港區": "cold",
    "文山區": "cold",
    "新店區": "cold",
    "汐止區": "cold",
    "大安區": "hot",
    "內湖特區": "normal",
}

selected_zones = ["中山區", "松山區", "大安區", "信義區",
                  "南港區", "新店區", "文山區", "汐止區", "內湖特區"]


# --------------------------------------------------

order_folder_path = 'test_order'


order_folder_path = 'test_order'
order_csv_file_name = '2021-10-04.csv'

test_obj = address_preprocess
order_df = test_obj.read_specific_csv(order_folder_path, order_csv_file_name)
order_df = test_obj.process_df(order_df)
order_df = test_obj.get_complete_address(order_df)

print(order_df)
