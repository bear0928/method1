import pandas as pd
import glob
import os
from typing import List
import warnings

warnings.filterwarnings("ignore")


class timetable:
    def __init__(
        self,
        allowed_zones: List,
        end_of_timeslots_bade: List,
        num_of_defered_timeslots: int,
    ):
        self.allowed_zones = allowed_zones
        self.num_of_zones = len(allowed_zones)
        self.end_of_timeslots = end_of_timeslots_bade
        self.num_of_timeslots = len(end_of_timeslots_bade)
        self.num_of_defered_timeslots = num_of_defered_timeslots

    def get_defered_timeslots(self) -> int:
        return self.num_of_defered_timeslots

    def modify_defered_timeslots(self, val: int) -> bool:
        if val < 0:
            return False
        else:
            self.num_of_defered_timeslots = val
            return True

    def process_date(self, df: pd.DataFrame) -> pd.DataFrame:
        for i in range(df["叫件時間"].count()):
            df["叫件時間"].iloc[i] = pd.to_datetime(df["叫件時間"].iloc[i]).time()
        return df

    def process_timeslot(self, df: pd.DataFrame) -> pd.DataFrame:
        df.insert(2, "timeslot", 0)
        for i in range(df["timeslot"].count()):
            curtime = df["叫件時間"].iloc[i]
            if curtime <= self.end_of_timeslots[0]:
                df["timeslot"].iloc[i] = 0
            else:
                for j in range(1, len(self.end_of_timeslots)):
                    if (
                        curtime > self.end_of_timeslots[j - 1]
                        and curtime <= self.end_of_timeslots[j]
                    ):
                        df["timeslot"].iloc[i] = j
                        break
        return df

    def process_csv_files(self, folder_path: str) -> pd.DataFrame:
        # 用glob取得所有符合指定路徑的CSV檔案列表
        file_list = glob.glob(folder_path)
        combined_data = pd.DataFrame()

        # 迭代處理每個CSV檔案
        for file in file_list:
            data = pd.read_csv(file)
            # 篩選出符合收件區域的資料
            # FIXME: 注意中山區在歷史資料裡沒有分成一區、二區
            filtered_data = data[data["收件區域"].isin(self.allowed_zones)]
            file_name = os.path.splitext(os.path.basename(file))[0]  # 提取檔案名稱中的數字部分
            filtered_data.loc[:, "日期"] = file_name
            # 將提取的資料合併到統計用的DataFrame物件
            combined_data = pd.concat([combined_data, filtered_data])
        return combined_data

    def calculate_statistics(
        self, df: pd.DataFrame, num_of_days: int, func: str
    ) -> tuple:
        avg_order_of_one = (
            (df.groupby([func])[func].count() / num_of_days)
            .to_frame(name="平均數")
            .reset_index()
        )
        avg_order_of_all = avg_order_of_one["平均數"].mean()
        std_dev = avg_order_of_one["平均數"].std()
        return avg_order_of_all, avg_order_of_one, std_dev

    def classify_zones(self, x_: pd.DataFrame, x__: float, std: float) -> dict:
        hot_zones = x_[x_["平均數"] > x__ + 0.4 * std].loc[:, "收件區域"].to_list()
        cold_zones = x_[x_["平均數"] < x__ - 0.4 * std].loc[:, "收件區域"].to_list()
        normal_zones = list(
            set(x_.loc[:, "收件區域"].to_list()) - set(hot_zones) - set(cold_zones)
        )

        zone_type = dict()
        for zone in hot_zones:
            zone_type[zone] = "hot"
        for zone in cold_zones:
            zone_type[zone] = "cold"
        for zone in normal_zones:
            zone_type[zone] = "normal"
        return zone_type

    def classify_timeslot(self, x_: pd.DataFrame, x__: float, std: float) -> dict:
        peak = x_[x_["平均數"] > x__ + 0.5 * std].loc[:, "timeslot"].to_list()
        offpeak = list(set(x_.loc[:, "timeslot"].to_list()) - set(peak))

        timeslot_type = dict()
        for slot in peak:
            timeslot_type[slot] = "peak"
        for slot in offpeak:
            timeslot_type[slot] = "offpeak"
        return timeslot_type

    def generate_timetable(self, history_df: pd.DataFrame) -> tuple:
        filtered_data = history_df[history_df["收件區域"].isin(allowed_zones)]
        # TODO: check condition
        filtered_data = self.process_date(filtered_data)
        # TODO: check condition
        filtered_data = self.process_timeslot(filtered_data)
        num_of_days = len(filtered_data["日期"].value_counts())
        (
            avg_order_of_all_zone,
            avg_order_of_a_zone,
            std_dev_of_zone,
        ) = self.calculate_statistics(filtered_data, num_of_days, "收件區域")
        zone_type = self.classify_zones(
            avg_order_of_a_zone, avg_order_of_all_zone, std_dev_of_zone
        )
        (
            avg_order_of_all_timeslot,
            avg_order_of_a_timeslot,
            std_dev_of_timeslot,
        ) = self.calculate_statistics(filtered_data, num_of_days, "timeslot")
        timeslot_type = self.classify_timeslot(
            avg_order_of_a_timeslot, avg_order_of_all_timeslot, std_dev_of_timeslot
        )
        return zone_type, timeslot_type


if __name__ == "__main__":
    allowed_zones = [
        # "中山一區",
        # "中山二區",
        "中山區",
        # "大同區",
        # "萬華區",
        # "中正區",
        # "中和區",
        # "永和區",
        # "板橋區",
        # "新莊區",
        # "土城區",
        # "士林區",
        # "北投區",
        # "林口區",
        # "三重區",
        # "蘆洲區",
        "松山區",
        "大安區",
        "信義區",
        "南港區",
        "新店區",
        "文山區",
        "汐止區",
        "內湖特區",
    ]
    end_of_timeslots_bade = list(
        map(
            lambda x: pd.to_datetime(x).time(),
            [
                "10:19:00",
                "11:39:00",
                "12:59:00",
                "14:39:00",
                "15:59:00",
                "17:19:00",
                "19:00:00",
            ],
        )
    )
    num_of_defered_timeslots = 1
    folder_path = "history_data/t1/*.csv"
    obj = timetable(allowed_zones, end_of_timeslots_bade, num_of_defered_timeslots)
    zone_his = obj.process_csv_files(folder_path)
    zone, timeslot = obj.generate_timetable(zone_his)
    print(zone, timeslot)
