import pandas as pd
from pandas import DataFrame as df
# from orm import database
from math import radians, sin, cos, sqrt, atan2
from datetime import datetime, timedelta


class dispatch:
    def __init__(self, hub_coor={"lat": 25.05021023163576, "lng": 121.56510795456256}):
        self.hub_coor = hub_coor

    def packing(self, order_df: df) -> df:
        order_df["fare"] = order_df["fare"].astype(int)
        packages = (
            order_df.groupby(["pickup_lng", "pickup_lat"])
            .agg({"floor": "size", "fare": "sum", "pickup_zone": "first", "id": set})
            .reset_index()
        )
        packages.rename(
            columns={"floor": "num_of_floors", "fare": "total_fare"}, inplace=True
        )
        packages["carrier_id"] = -1
        packages["status"] = "unassign"
        return packages

    def packing_v2(self, order_df: pd.DataFrame) -> pd.DataFrame:
        order_df["fare"] = order_df["fare"].astype(int)
        packages = (
            order_df.groupby(["id"])
            .agg({
                "pickup_lng": "first",
                "pickup_lat": "first",
                "floor": "size",
                "fare": "sum",
                "pickup_zone": "first",
            })
            .reset_index()
        )
        packages.rename(
            columns={"floor": "num_of_floors", "fare": "total_fare"}, inplace=True
        )
        packages["carrier_id"] = -1
        packages["status"] = "unassign"

        # 使用 apply 將 id 轉換為 list
        packages["id"] = packages["id"].apply(lambda x: [x])

        return packages

    def dont_packing(self, order_df):
        order_df["fare"] = order_df["fare"].astype(int)
        packages_df = order_df.copy()
        packages_df['num_of_floors'] = 1
        packages_df['carrier_id'] = -1
        packages_df['status'] = 'unassign'
        packages_df.rename(columns={'fare': 'total_fare'}, inplace=True)
        return packages_df

    def calculate_distance(self, lat1, lng1, lat2, lng2):
        R = 6371.0  # Radius of the Earth in kilometers
        lat1_rad = radians(lat1)
        lng1_rad = radians(lng1)
        lat2_rad = radians(lat2)
        lng2_rad = radians(lng2)

        dlng = lng2_rad - lng1_rad
        dlat = lat2_rad - lat1_rad

        a = sin(dlat / 2) ** 2 + cos(lat1_rad) * \
            cos(lat2_rad) * sin(dlng / 2) ** 2
        c = 2 * atan2(sqrt(a), sqrt(1 - a))

        distance = R * c
        return distance

    def calculate_driving_time(
        self, c_lng, c_lat, p_lng, p_lat, hub_lat, hub_lng, speed
    ):
        c_lng, c_lat, p_lng, p_lat, hub_lng, hub_lat = (
            float(c_lng),
            float(c_lat),
            float(p_lng),
            float(p_lat),
            float(hub_lng),
            float(hub_lat),
        )

        # 計算 c 到 p 以及 p 到 hub 的距離
        c_to_p_distance = self.calculate_distance(c_lat, c_lng, p_lat, p_lng)
        p_to_hub_distance = self.calculate_distance(
            p_lat, p_lng, hub_lat, hub_lng)

        # 計算 c 到 p 以及 p 到 hub 的行車時間
        c_to_p_time = c_to_p_distance / speed
        p_to_hub_time = p_to_hub_distance / speed

        # 總行車時間為 c 到 p 的時間加上 p 到 hub 的時間
        total_time = c_to_p_time + p_to_hub_time

        # 將行車時間轉換成 timedelta 格式
        total_time = timedelta(minutes=total_time)

        c_to_p_time = timedelta(minutes=c_to_p_time)

        return total_time, c_to_p_time

    # 判斷是否能準時回站
    def check_time_conditions(self, dp_time, total_time, time_slot_end, package):
        # 比較是否在時間範圍內
        dp_datetime = datetime(
            1999, 9, 28, dp_time.hour, dp_time.minute, dp_time.second
        )

        # 將時間加上總時間，以確定是否在時間範圍內
        new_datetime = dp_datetime + total_time + \
            timedelta(minutes=1 * package['num_of_floors'])
        # print(new_datetime)
        # 比較是否在時間範圍內
        if new_datetime.time() <= time_slot_end:
            # print('v')
            return 1
        else:
            return 0

    def assign_paks_to_couriers(
        self, packages: df, carriers: df, time_limit: pd.Timestamp, func: str = "normal"
    ) -> tuple:
        for idx, pak in packages.iterrows():
            # print('-')
            if func == "normal":
                carry = carriers.loc[carriers["zone"] == pak["pickup_zone"]]
            else:
                carry = carriers
            carry = carry.sort_values("salary")
            for c_idx, carrier in carry.iterrows():
                # print(carrier['id'])
                total_time, c_to_p_time = self.calculate_driving_time(
                    carrier["lng"],
                    carrier["lat"],
                    pak["pickup_lng"],
                    pak["pickup_lat"],
                    self.hub_coor["lat"],
                    self.hub_coor["lng"],
                    carrier["speed"],
                )
                if self.check_time_conditions(
                    carrier["avtime"], total_time, time_limit, pak
                ):
                    # print(carrier['id'])
                    packages.at[idx, "carrier_id"] = carrier["id"]
                    packages.at[idx, "status"] = "assigned"
                    carriers.at[c_idx, "lat"] = pak["pickup_lat"]
                    carriers.at[c_idx, "lng"] = pak["pickup_lng"]
                    carriers.at[c_idx, "salary"] += pak["total_fare"]
                    tmp_datetime = datetime(
                        1999,
                        9,
                        28,
                        carrier["avtime"].hour,
                        carrier["avtime"].minute,
                        carrier["avtime"].second,
                    )
                    new_avtime = (tmp_datetime + c_to_p_time +
                                  timedelta(minutes=1 * pak['num_of_floors'])).time()

                    # print(c_to_p_time)
                    carriers.at[c_idx, "avtime"] = new_avtime
                    break
        return carriers, packages


if __name__ == "__main__":
    db = database("localhost", "root", "password", "express_system")
    order_df = db.get_all_orders_as_df()
    # print(type(order_df))
    # print(order_df)
    dis = dispatch()
    package = dis.packing(order_df)
    # print(package)
    carriers = {
        "id": [10, 12, 32, 74, 50, 36, 7, 82, 19, 101, 211, 2, 13],
        "zone": [
            "松山區",
            "松山區",
            "中和區",
            "信義區",
            "大安區",
            "中山區",
            "中山區",
            "中山區",
            "板橋區",
            "內湖區",
            "大同區",
            "南港區",
            "萬華區",
        ],
    }
    carriers_df = pd.DataFrame(carriers)
    carriers_df["lng"] = "121.5651079"
    carriers_df["lat"] = "25.0502102"
    carriers_df["salary"] = 0
    carriers_df["speed"] = 0.25
    carriers_df["avtime"] = pd.to_datetime("08:00:00").time()
    # print(carriers_df)
    c, p = dis.assign_paks_to_couriers(
        package, carriers_df, pd.to_datetime("09:00:00").time()
    )
    # print(c)
    # print("--------------------------------------------")
    # print(p)
    c.to_csv("c.csv")
    p.to_csv("p.csv")
