import abc
import mysql.connector
import pandas as pd


class base(metaclass=abc.ABCMeta):
    def __init__(self):
        pass

    @abc.abstractmethod
    def get_all_orders_as_df(self):
        return NotImplemented

    @abc.abstractmethod
    def get_orders_by_ids(self, ids):
        return NotImplemented

    @abc.abstractmethod
    def insert_pak(self, ids):
        return NotImplemented

    @abc.abstractmethod
    def update_pak_id_in_orders_by_ids(self, pak_id, ids):
        return NotImplemented

    @abc.abstractmethod
    def get_sum_of_fare_in_order_by_pak_id(self, pak_id):
        return NotImplemented

    @abc.abstractmethod
    def update_pay_in_pak_by_id(self, pay, id):
        return NotImplemented

    @abc.abstractmethod
    def update_num_of_floors_in_pak_by_id(self, num_of_floors, id):
        return NotImplemented

    @abc.abstractmethod
    def count_floor_from_order_by_pak_id(self, pak_id):
        return NotImplemented

    @abc.abstractmethod
    def get_name_from_zone_by_hub_and_t1_zone_type(self, hub_name, zone_type):
        return NotImplemented

    @abc.abstractmethod
    def get_unassigned_id_from_pak_by_zone(self, zone_name):
        return NotImplemented

    @abc.abstractmethod
    def get_working_id_from_courier_by_zone(self, zone_name):
        return NotImplemented

    @abc.abstractmethod
    def get_a_pak_id_from_courier_schedule_by_courier_id(self, courier_id):
        return NotImplemented

    @abc.abstractmethod
    def get_lng_lat_from_pak_py_id(self, pak_id):
        return NotImplemented

    @abc.abstractmethod
    def get_a_departure_time_from_courier_schedule_by_courier_id(self, courier_id):
        return NotImplemented

    @abc.abstractmethod
    def get_num_of_pak_from_courier_schedule_by_courier_id(self, courier_id):
        return NotImplemented

    @abc.abstractmethod
    def get_a_courier_by_id(self, courier_id):
        return NotImplemented

    @abc.abstractmethod
    def get_a_packing_time_from_pak_by_id(self, pak_id):
        return NotImplemented

    @abc.abstractmethod
    def insert_courier_schedule(self, values):
        return NotImplemented


class database(base):
    def __init__(self, host, user, password, db_name):
        self.conn = mysql.connector.connect(
            host=host, user=user, password=password, database=db_name
        )

    def __del__(self):
        self.conn.close()

    def get_all_orders_as_df(self):
        query = "SELECT id, date, DATE_FORMAT(submission_time, '%H:%i:%s') AS submission_time, pickup_zone, pickup_address, pickup_lng, pickup_lat, `floor`, pak_id, fare, cus_id, delivery_zone, delivery_address FROM `order`"
        orders_df = pd.read_sql_query(query, self.conn)
        return orders_df

    def get_orders_by_ids(self, ids):
        cursor = self.conn.cursor()
        query = "SELECT pickup_zone, pickup_lng, pickup_lat FROM `order` WHERE id IN ({})".format(
            ",".join(["%s"] * len(ids))
        )
        cursor.execute(query, ids)
        zones = cursor.fetchall()
        cursor.close()
        return zones

    def insert_pak(self, values):
        cursor = self.conn.cursor()
        query = "INSERT INTO `pak` (`zone`,`pickup_lng`, `pickup_lat`, `packing_time`, `due_time`, `pickup_courier`, `assigned`, `pickup_is_delayed`) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"
        cursor.execute(query, values)
        pak_id = cursor.lastrowid
        self.conn.commit()
        cursor.close()
        return pak_id

    def update_pak_id_in_orders_by_ids(self, pak_id, ids):
        cursor = self.conn.cursor()
        query = "UPDATE `order` SET `pak_id` = %s WHERE id IN ({})".format(
            ",".join(["%s"] * len(ids))
        )
        values = [pak_id] + ids
        cursor.execute(query, values)
        self.conn.commit()
        cursor.close()

    def get_sum_of_fare_in_order_by_pak_id(self, pak_id):
        cursor = self.conn.cursor()
        query = "SELECT SUM(`fare`) FROM `order` WHERE pak_id = %s"
        cursor.execute(query, (pak_id,))
        total_fare = cursor.fetchone()[0]
        cursor.close()
        return total_fare

    def update_pay_in_pak_by_id(self, pay, id):
        cursor = self.conn.cursor()
        query = "UPDATE `pak` SET pay = %s WHERE id = %s"
        cursor.execute(query, (pay, id))
        self.conn.commit()
        cursor.close()

    def update_num_of_floors_in_pak_by_id(self, num_of_floors, id):
        cursor = self.conn.cursor()
        query = "UPDATE `pak` SET number_of_floors = %s WHERE `id` = %s"
        cursor.execute(query, (num_of_floors, id))
        self.conn.commit()
        cursor.close()

    def count_floor_from_order_by_pak_id(self, pak_id):
        cursor = self.conn.cursor()
        query = "SELECT COUNT(DISTINCT `floor`) FROM `order` WHERE `pak_id` = %s"
        cursor.execute(query, (pak_id,))
        num_floors = cursor.fetchone()[0]
        cursor.close()
        return num_floors

    def get_name_from_zone_by_hub_and_t1_zone_type(self, hub_name, zone_type):
        cursor = self.conn.cursor()
        query = "SELECT `name` FROM `zone` WHERE `hub` = %s AND `t1_zone_type` = %s"
        values = (hub_name, zone_type)
        cursor.execute(query, values)
        result = cursor.fetchall()
        cursor.close()
        return [zone[0] for zone in result]

    def get_unassigned_id_from_pak_by_zone(self, zone_name):
        cursor = self.conn.cursor()
        query = "SELECT `id` FROM `pak` WHERE `zone` = %s AND `assigned` = 0"
        values = (zone_name,)
        cursor.execute(query, values)
        result = cursor.fetchall()
        # 將 unassigned_paks 轉成 list
        result = [pak[0] for pak in result]
        cursor.close()
        return result

    def get_working_id_from_courier_by_zone(self, zone_name):
        cursor = self.conn.cursor()
        query = "SELECT `id` FROM `courier` WHERE `zone` = %s AND `is_working` = 1"
        values = (zone_name,)
        cursor.execute(query, values)
        result = cursor.fetchall()
        # 將 working_couriers 轉成 list
        result = [courier[0] for courier in result]
        cursor.close()
        return result

    def get_a_pak_id_from_courier_schedule_by_courier_id(self, courier_id):
        cursor = self.conn.cursor()

        # 查詢某一車手的最新一個 courier_schedule
        query = """
            SELECT `pak_id`
            FROM `courier_schedule`
            WHERE courier_id = %s
            ORDER BY num_of_pak DESC
            LIMIT 1
        """
        cursor.execute(query, (courier_id,))
        result = cursor.fetchone()
        cursor.close()
        return result

    def get_lng_lat_from_pak_py_id(self, pak_id):
        cursor = self.conn.cursor()
        query = "SELECT pickup_lng, pickup_lat FROM pak WHERE id = %s"
        cursor.execute(query, (pak_id,))
        result = cursor.fetchone()
        cursor.close()
        return result

    def get_a_departure_time_from_courier_schedule_by_courier_id(self, courier_id):
        cursor = self.conn.cursor()
        # 查詢符合條件的資料，按照 num_of_pak 降序排序，並只取第一筆
        query = "SELECT completion_time FROM courier_schedule WHERE courier_id = %s ORDER BY num_of_pak DESC LIMIT 1"
        cursor.execute(query, (courier_id,))
        result = cursor.fetchone()
        cursor.close()
        return result

    def get_num_of_pak_from_courier_schedule_by_courier_id(self, courier_id):
        cursor = self.conn.cursor()
        # 查詢符合條件的資料，按照 num_of_pak 降序排序，並只取第一筆
        query = "SELECT MAX(num_of_pak) FROM courier_schedule WHERE courier_id = %s ORDER BY num_of_pak DESC LIMIT 1"
        cursor.execute(query, (courier_id,))
        result = cursor.fetchone()
        cursor.close()
        return result

    def get_a_courier_by_id(self, courier_id):
        cursor = self.conn.cursor()
        query_courier = "SELECT * FROM `courier` WHERE `id` = %s"
        cursor.execute(query_courier, (courier_id,))
        result = cursor.fetchone()
        cursor.close()
        return result

    def get_a_packing_time_from_pak_by_id(self, pak_id):
        cursor = self.conn.cursor()
        query_pak = "SELECT `packing_time` FROM `pak` WHERE `id` = %s"
        cursor.execute(query_pak, (pak_id,))
        result = cursor.fetchone()
        cursor.close()
        return result

    def insert_courier_schedule(self, values):
        cursor = self.conn.cursor()
        # 根據參數構建 INSERT 語句
        query = (
            "INSERT INTO `courier_schedule` "
            "(date, time_slot, courier_id, num_of_pak, pak_id, completion_time) "
            "VALUES (%s, %s, %s, %s, %s, %s)"
        )
        cursor.execute(query, values)
        self.conn.commit()
        cursor.close()


# class testdb(base):
#     def __init__(self):
#         pass

#     def get_all_orders_as_df(self):
#         data = [["Google", 10], ["Runoob", 12], ["Wiki", 13]]
#         orders_df = pd.DataFrame(data, columns=["Site", "Age"], dtype=float)
#         return orders_df

#     def get_orders_by_ids(self, ids):
#         zones = [
#             ("文山區", "121.566707", "24.988706"),
#             ("中山區", "121.5211361", "25.0620534"),
#         ]
#         return zones

#     def insert_pak(self, values):
#         print(values)
#         return 2

#     def update_pak_id_in_orders_by_ids(self, pak_id, ids):
#         print(pak_id)
#         print(ids)

#     def get_sum_of_fare_in_order_by_pak_id(self, pak_id):
#         total_fare = 1
#         return total_fare

#     def update_pay_in_pak_by_id(self, pay, id):
#         print(pay)
#         print(id)

#     def update_num_of_floors_in_pak_by_id(self, num_of_floors, id):
#         print(num_of_floors)
#         print(id)

#     def count_floor_from_order_by_pak_id(self, pak_id):
#         num_floors = 1
#         return num_floors

#     def get_name_from_zone_by_hub_and_t1_zone_type(self, hub_name, zone_type):
#         result = [[1, 2], [1, 2]]
#         return [zone[0] for zone in result]

#     def get_unassigned_id_from_pak_by_zone(self, zone_name):
#         result = [[1, 2], [1, 2]]
#         result = [pak[0] for pak in result]
#         return result

#     def get_working_id_from_courier_by_zone(self, zone_name):
#         result = [[1, 2], [1, 2]]
#         result = [courier[0] for courier in result]
#         return result

#     def get_a_pak_id_from_courier_schedule_by_courier_id(self, courier_id):
#         result = 1
#         return result

#     def get_lng_lat_from_pak_py_id(self, pak_id):
#         result = 1
#         return result

#     def get_a_departure_time_from_courier_schedule_by_courier_id(self, courier_id):
#         result = 1
#         return result

#     def get_a_courier_by_id(self, courier_id):
#         result = 1
#         return result

#     def get_a_packing_time_from_pak_by_id(self, pak_id):
#         result = 1
#         return result

#     def insert_courier_schedule(self, values):
#         print(values)


if __name__ == "__main__":
    db = database("localhost", "root", "password", "express_system")
    print(db.get_orders_by_ids(["C2110050277", "C2110120381"]))
