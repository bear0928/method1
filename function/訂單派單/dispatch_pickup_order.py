import mysql.connector


def get_orders_before_time(date, time_str_1, time_str_2):
    connection = mysql.connector.connect(
        host="localhost",
        user="root",
        password="password",
        database="express_system",
    )

    try:
        with connection.cursor() as cursor:
            # SQL查詢語句，擷取指定日期(date)且時間(time)早於指定時間(time_str)的訂單
            sql_query = f"SELECT * FROM `orders` WHERE `date`='{date}' AND `submission_time` < '{time_str_2}' AND `submission_time` > '{time_str_1}';"

            # 執行查詢
            cursor.execute(sql_query)

            # 取得查詢結果
            result = cursor.fetchall()

            # 回傳查詢結果
            return result, time_str_2
    finally:
        # 關閉資料庫連接
        connection.close()
