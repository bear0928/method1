import pandas as pd
from datetime import timedelta, datetime, time
from orm import database
from math import radians, sin, cos, sqrt, atan2
import random


class dispatch:
    def __init__(
        self,
        time_slot,
        zones,
        zone_type,
        db: database,
        hub_coor={"lat": 25.05021023163576, "lng": 121.56510795456256},
    ):
        self.db = db
        self.time_slot = time_slot
        self.hub_coor = hub_coor
        self.zones = zones
        self.zone_type = zone_type

    # pak the orders with the same coor
    def package_orders(self, orders_df):
        packages = {}
        for _, order in orders_df.iterrows():
            pickup_lng = order["pickup_lng"]
            pickup_lat = order["pickup_lat"]
            package_key = f"{pickup_lng},{pickup_lat}"
            if package_key in packages:
                packages[package_key].append(order["id"])
            else:
                packages[package_key] = [order["id"]]

        return packages

    def save_pak_data(self, packages, packing_time):
        # FIXME: 打包的裡面總共有幾個訂單
        for packages, order_ids in packages.items():
            # 要轉換的日期和時間字符串（注意加入年份）
            date_time_str = "2023/10/13 10:20:00"
            # 定義日期和時間字符串的格式
            date_time_format = "%Y/%m/%d %H:%M:%S"
            # 使用 datetime.strptime() 方法進行轉換
            due_time = datetime.strptime(date_time_str, date_time_format)

            orders = self.db.get_orders_by_ids(order_ids)
            zone = orders[0][0]
            lng = orders[0][1]
            lat = orders[0][2]

            assigned = 0
            pickup_courier = None
            pickup_is_delayed = 0

            # Insert data into `pak`
            values = (
                zone,
                lng,
                lat,
                packing_time,
                due_time,
                pickup_courier,
                assigned,
                pickup_is_delayed,
            )
            pak_id = self.db.insert_pak(values=values)

            # Update `pak_id` in `order` table for the corresponding orders
            self.db.update_pak_id_in_orders_by_ids(pak_id=pak_id, ids=order_ids)

            # Calculate total fare of orders in the pak
            total_fare = self.db.get_sum_of_fare_in_order_by_pak_id(pak_id=pak_id)

            # Calculate pay and update `pay` in `pak` table
            pay = total_fare * 0.4
            # FIXME: pay要如何計算
            self.db.update_pay_in_pak_by_id(pay=pay, id=pak_id)

            # Calculate number of unique floors in the pak's orders
            num_floors = self.db.count_floor_from_order_by_pak_id(pak_id=pak_id)

            # Update `number_of_floors` in `pak` table
            self.db.update_num_of_floors_in_pak_by_id(
                num_of_floors=num_floors, id=pak_id
            )

    def find_latest_courier_coor(self, courier_id):
        # 查詢某一車手的最新一個 courier_schedule
        result = self.db.get_a_pak_id_from_courier_schedule_by_courier_id(courier_id)

        if result:
            pak_id = result
            # 查詢 pak 中的 pickup_lng 和 pickup_lat
            courier_lng, courier_lat = self.db.get_lng_lat_from_pak_py_id(pak_id)
        else:
            # 若找不到最新的 courier_schedule，回傳 hub 的座標
            hub_lat = self.hub_coor["lat"]
            hub_lng = self.hub_coor["lng"]
            courier_lng, courier_lat = hub_lng, hub_lat

        return courier_lng, courier_lat

    def find_pickup_lng_lat_by_pak_id(self, pak_id):
        # 查詢 pak 的 pickup_lng 和 pickup_lat
        result = self.db.get_lng_lat_from_pak_py_id(pak_id)

        if result:
            pickup_lng, pickup_lat = result
        else:
            # 若找不到相應的 pak，回傳 None
            pickup_lng, pickup_lat = None, None

        return pickup_lng, pickup_lat

    # 查詢車手出發前往收件地點的時間
    def find_start_time_by_courier_id(self, courier_id, pak_id, default_time):
        # 查詢符合條件的資料，按照 num_of_pak 降序排序，並只取第一筆
        result = self.db.get_a_departure_time_from_courier_schedule_by_courier_id(
            courier_id
        )

        if result:
            start_time = result[0]
            return start_time
        else:
            # 如果在 courier_schedule 中找不到，檢查是否有該車手在 courier 中
            courier_result = self.db.get_a_courier_by_id(courier_id)

            if courier_result:
                # 如果有該車手，則從 pak 表中取得打包時間
                pak_result = self.db.get_a_packing_time_from_pak_by_id(pak_id)

                if pak_result:
                    # 如果有打包時間，則回傳該打包時間
                    return pak_result[0]
                else:
                    return default_time
            else:
                return default_time

    def calculate_distance(self, lat1, lng1, lat2, lng2):
        # FIXME: 可以考慮使用曼哈頓距離
        R = 6371.0  # Radius of the Earth in kilometers
        lat1_rad = radians(lat1)
        lng1_rad = radians(lng1)
        lat2_rad = radians(lat2)
        lng2_rad = radians(lng2)

        dlng = lng2_rad - lng1_rad
        dlat = lat2_rad - lat1_rad

        a = sin(dlat / 2) ** 2 + cos(lat1_rad) * cos(lat2_rad) * sin(dlng / 2) ** 2
        c = 2 * atan2(sqrt(a), sqrt(1 - a))

        distance = R * c
        return distance

    def calculate_driving_time(
        self, c_lng, c_lat, p_lng, p_lat, hub_lat, hub_lng, speed
    ):
        c_lng, c_lat, p_lng, p_lat, hub_lng, hub_lat = (
            float(c_lng),
            float(c_lat),
            float(p_lng),
            float(p_lat),
            float(hub_lng),
            float(hub_lat),
        )

        # 計算 c 到 p 以及 p 到 hub 的距離
        c_to_p_distance = self.calculate_distance(c_lat, c_lng, p_lat, p_lng)
        p_to_hub_distance = self.calculate_distance(p_lat, p_lng, hub_lat, hub_lng)

        # 計算 c 到 p 以及 p 到 hub 的行車時間
        c_to_p_time = c_to_p_distance / speed
        p_to_hub_time = p_to_hub_distance / speed

        # 總行車時間為 c 到 p 的時間加上 p 到 hub 的時間
        total_time = c_to_p_time + p_to_hub_time

        # 將行車時間轉換成 timedelta 格式
        total_time = timedelta(minutes=total_time)
        c_to_p_time = timedelta(minutes=c_to_p_time)

        # print("總行車時間（時:分:秒）:", total_time)
        # print("c 到 p 的行車時間（時:分:秒）:", c_to_p_time)

        return total_time, c_to_p_time

    # 判斷是否能準時回站
    def check_time_conditions(self, dp_time, total_time, time_slot_end):
        # 將時間轉換為 datetime.datetime 類型，設定日期為 2023/10/13
        dp_datetime = datetime(
            2023, 10, 13, dp_time.hour, dp_time.minute, dp_time.second
        )

        # 將時間加上總時間，以確定是否在時間範圍內
        new_datetime = dp_datetime + total_time

        # 比較是否在時間範圍內
        if new_datetime.time() <= time_slot_end:
            return 1
        else:
            return 0

    def assign_paks_to_couriers(self, date, time_slot_index, speed, zone):
        hub_lat = self.hub_coor["lat"]
        hub_lng = self.hub_coor["lng"]
        unassigned_paks = self.db.get_unassigned_id_from_pak_by_zone(zone)
        working_couriers = self.db.get_working_id_from_courier_by_zone(zone)
        number_of_courier = len(working_couriers)
        print(unassigned_paks)
        print(working_couriers)

        cannot_assigned_pak = []
        courier_index = random.randint(0, number_of_courier)
        for pak in unassigned_paks:
            check = 0
            while check != number_of_courier:
                p_lng, p_lat = self.db.get_lng_lat_from_pak_py_id(pak)
                c_lng, c_lat = self.find_latest_courier_coor(
                    working_couriers[courier_index]
                )
                total_time, cp_time = self.calculate_driving_time(
                    c_lng, c_lat, p_lng, p_lat, hub_lat, hub_lng, speed
                )
                st_time = self.find_start_time_by_courier_id(
                    courier_index, pak, self.time_slot[time_slot_index - 1]
                )
                find = self.check_time_conditions(
                    st_time, total_time, self.time_slot[time_slot_index]
                )
                if find == 1:
                    st_datetime = datetime(
                        2023, 10, 13, st_time.hour, st_time.minute, st_time.second
                    )
                    completion_time = st_datetime + cp_time
                    num_of_pak = (
                        self.db.get_num_of_pak_from_courier_schedule_by_courier_id(
                            courier_index
                        )
                    )
                    self.db.insert_courier_schedule(
                        date,
                        time_slot_index,
                        courier_index,
                        num_of_pak,
                        pak,
                        completion_time,
                    )
                    break
                else:
                    check += 1
                courier_index = (courier_index + 1) % number_of_courier

            else:
                cannot_assigned_pak.append(pak)

        return cannot_assigned_pak


# def assign_all_paks():


if __name__ == "__main__":
    db = database("localhost", "root", "password", "express_system")
    order_df = db.get_all_orders_as_df()
    time_slot = list(
        map(
            lambda x: pd.to_datetime(x).time(),
            [
                "08:00:00",
                "08:05:00",
                "12:59:00",
                "14:39:00",
                "15:59:00",
                "17:19:00",
                "19:00:00",
            ],
        )
    )
    zones = [
        "中山一區",
        "中山二區",
        "中山區",
        "大同區",
        "萬華區",
        "中正區",
        "中和區",
        "永和區",
        "板橋區",
        "新莊區",
        "土城區",
        "士林區",
        "北投區",
        "林口區",
        "三重區",
        "蘆洲區",
        "松山區",
        "大安區",
        "信義區",
        "南港區",
        "新店區",
        "文山區",
        "汐止區",
        "內湖特區",
    ]
    zone_type = {
        "中山區": "hot",
        "信義區": "hot",
        "內湖特區": "hot",
        "大安區": "hot",
        "松山區": "hot",
        "三重區": "cold",
        "中和區": "cold",
        "北投區": "cold",
        "土城區": "cold",
        "士林區": "cold",
        "文山區": "cold",
        "新店區": "cold",
        "永和區": "cold",
        "汐止區": "cold",
        "蘆洲區": "cold",
        "南港區": "normal",
        "中正區": "normal",
        "新莊區": "normal",
        "萬華區": "normal",
        "板橋區": "normal",
        "大同區": "normal",
    }
    dis = dispatch(time_slot, zones, zone_type, db)
    packages = dis.package_orders(order_df)
    packing_time = datetime(2023, 10, 13, 9, 55, 00)
    dis.save_pak_data(packages, packing_time)
    cannot_assigned_pak = dis.assign_paks_to_couriers("10:13", 0, 15, "松山區")
    print(cannot_assigned_pak)
