# %%
import pandas as pd
import glob
import re
import requests
import json
import math
import mysql.connector


class address_preprocess:
    def __init__(self):
        self.allowed_zones = [
            "中山一區",
            "中山二區",
            "中山區",
            # 中山區!!!
            "大同區",
            "萬華區",
            "中正區",
            "中和區",
            "永和區",
            "板橋區",
            "新莊區",
            "土城區",
            "士林區",
            "北投區",
            "林口區",
            "三重區",
            "蘆洲區",
            "松山區",
            "大安區",
            "信義區",
            "南港區",
            "新店區",
            "文山區",
            "汐止區",
            "內湖特區",
        ]
        self.mailingAddressList = []
        self.count = 0

    def get_complete_address(self, order_data):
        self.mailingAddressList = []
        self.count = 0
        order_data = self.filter_orders_by_zone(order_data)
        mailingRegion, mailingAddressUncomplete = self.extract_pickup_data(order_data)
        self.recursive(mailingAddressUncomplete, mailingRegion)
        self.fill_mailing_address(order_data)
        self.extract_floor(order_data)
        # self.add_coor(order_data)
        return order_data

    def filter_orders_by_zone(self, order_data):
        # 創建一個空的DataFrame來存放符合條件的資料
        data = pd.DataFrame()
        filtered_data = order_data[order_data["收件區域"].isin(self.allowed_zones)]
        data = pd.concat([data, filtered_data], ignore_index=True)
        return data

    def extract_pickup_data(self, order_data):
        # 從order_data中提取收件區域和收件地址(不完整)欄位
        pickup_zone = order_data["收件區域"]
        pickup_address = order_data["收件地址(不完整)"]

        return pickup_zone, pickup_address

    def fill_mailing_address(self, order_data):
        # 確保self.mailingAddressList和order_data的行數相同
        if len(self.mailingAddressList) != len(order_data):
            raise ValueError("self.mailingAddressList和order_data的行數不相符")

        # 將self.mailingAddressList放入order_data的收件地址(補全)欄位
        order_data["收件地址(補全)"] = self.mailingAddressList

    def extract_floor(self, order_data):
        # 使用正則表達式提取樓層信息
        addresses = order_data["收件地址(補全)"]
        pattern = r"號(\d+[a-zA-Z]?)[樓Ff]"
        floors = []
        for address in addresses:
            match = re.search(pattern, address)
            if match:
                floors.append(match.group(1))
            else:
                floors.append("None")
        if len(floors) != len(order_data):
            raise ValueError("floors和order_data的行數不相符")
        order_data["收件樓層"] = floors

    def add_coor(self, order_data):
        # 使用正則表達式提取樓層信息
        addresses = order_data["收件地址(補全)"]
        lngs = []
        lats = []
        for address in addresses:
            coor = self.addrToCoordination(address)
            lngs.append(coor[0])
            lats.append(coor[1])
        order_data["lng"] = lngs
        order_data["lat"] = lats

    def getDistance(self, coorA: list, coorB: list):
        latA = coorA[1]
        lonA = coorA[0]
        latB = coorB[1]
        lonB = coorB[0]
        if latA == latB and lonA == lonB:
            return 0
        ra = 6378140  # 赤道半徑
        rb = 6356755  # 極半徑
        flatten = (ra - rb) / ra  # Partial rate of the earth
        # change angle to radians
        radLatA = math.radians(latA)
        radLonA = math.radians(lonA)
        radLatB = math.radians(latB)
        radLonB = math.radians(lonB)
        pA = math.atan(rb / ra * math.tan(radLatA))
        pB = math.atan(rb / ra * math.tan(radLatB))
        x = math.acos(
            math.sin(pA) * math.sin(pB)
            + math.cos(pA) * math.cos(pB) * math.cos(radLonA - radLonB)
        )
        c1 = (
            (math.sin(x) - x)
            * (math.sin(pA) + math.sin(pB)) ** 2
            / math.cos(x / 2) ** 2
        )
        c2 = (
            (math.sin(x) + x)
            * (math.sin(pA) - math.sin(pB)) ** 2
            / math.sin(x / 2) ** 2
        )
        dr = flatten / 8 * (c1 - c2)
        distance = ra * (x + dr)
        distance = round(distance / 1000, 4)
        return distance

    def addrToCoordination(self, address):
        url = (
            "https://maps.googleapis.com/maps/api/geocode/json?address="
            + address
            + "&key=AIzaSyB0myPoG4SqLtVbofN1l7HWscj7mNAd6cg"
        )
        res = requests.get(url)
        js = json.loads(res.text)
        result = js["results"][0]["geometry"]["location"]
        lat = result["lat"]
        lng = result["lng"]
        return list(map(lambda x: float(x), [lng, lat]))

    def travelTime(self, addrA: str, addrB: str, method=getDistance, velocity=0.25):
        coorA = self.addrToCoordination(addrA)
        coorB = self.addrToCoordination(addrB)
        dis = self.getDistance(coorA, coorB)
        return dis / velocity

    def insert(self, data, connection):
        if data["cus_id"] == "None":
            insert_query = """
            INSERT INTO `order` (`id`, date, submission_time, pickup_zone, pickup_address, pickup_lng, pickup_lat, floor, fare, delivery_zone, delivery_address)
            VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
            """

            values = (
                data["id"],
                "10/13",
                data["submission_time"],
                data["pickup_zone"],
                data["pickup_address"],
                data["pickup_lng"],
                data["pickup_lat"],
                data["floor"],
                data["fare"],
                data["delivery_zone"],
                data["delivery_address"],
            )
        else:
            insert_query = """
            INSERT INTO `order` (`id`, date, submission_time, pickup_zone, pickup_address, pickup_lng, pickup_lat, floor, fare, cus_id, delivery_zone, delivery_address)
            VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
            """

            values = (
                data["id"],
                "10/13",
                data["submission_time"],
                data["pickup_zone"],
                data["pickup_address"],
                data["pickup_lng"],
                data["pickup_lat"],
                data["floor"],
                data["fare"],
                data["cus_id"],
                data["delivery_zone"],
                data["delivery_address"],
            )

        cursor = connection.cursor()
        cursor.execute(insert_query, values)
        connection.commit()
        # 關閉連線
        cursor.close()

    def insert_all(self, order_data):
        connection = mysql.connector.connect(
            host="localhost",
            user="root",
            password="password",
            database="express_system",
        )

        for _, order in order_data.iterrows():
            # print(type(order))
            data = {}
            data["id"] = order["案件序號"]
            data["submission_time"] = order["叫件時間"]
            data["pickup_zone"] = order["收件區域"]
            data["pickup_address"] = order["收件地址(補全)"]
            data["pickup_lng"] = order["lng"]
            data["pickup_lat"] = order["lat"]
            data["floor"] = order["收件樓層"]
            data["fare"] = order["合計運費"]
            data["cus_id"] = "None"
            data["delivery_zone"] = order["送件區域"]
            data["delivery_address"] = order["送件地址(不完整)"]
            self.insert(data, connection)

        connection.close()

    def save_order_data_to_csv(self, order_data, file_path):
        # 將order_data輸出成CSV檔案，覆蓋原本的CSV檔案
        order_data.to_csv(file_path, index=False)

    def regionAdded(self, mailingAddress, mailingRegion):
        # print(mailingRegion)
        if mailingAddress.find("號") == -1:
            if mailingAddress.find("新北市") != -1:
                # print(mailingAddress)
                self.mailingAddressList.append(mailingAddress + "號")
            elif mailingAddress.find("台北市") != -1:
                # print(mailingAddress)
                self.mailingAddressList.append(mailingAddress + "號")
            elif mailingAddress.find("臺北市") != -1:
                # print(mailingAddress)
                self.mailingAddressList.append(mailingAddress + "號")
            elif (
                "三重區" == mailingRegion[self.count - 1]
                or "土城區" == mailingRegion[self.count - 1]
                or "中和區" == mailingRegion[self.count - 1]
                or "永和區" == mailingRegion[self.count - 1]
                or "汐止區" == mailingRegion[self.count - 1]
                or "板橋區" == mailingRegion[self.count - 1]
                or "林口區" == mailingRegion[self.count - 1]
                or "新店區" == mailingRegion[self.count - 1]
                or "新莊區" == mailingRegion[self.count - 1]
                or "蘆洲區" == mailingRegion[self.count - 1]
                or "五股區" == mailingRegion[self.count - 1]
                or "樹林區" == mailingRegion[self.count - 1]
                or "淡水區" == mailingRegion[self.count - 1]
                or "重莊特區" == mailingRegion[self.count - 1]
                and mailingAddress.find("新北市") == -1
            ):
                # print("新北市" + mailingAddress)
                self.mailingAddressList.append("新北市" + mailingAddress + "號")
            elif (
                "三重區" == mailingRegion[self.count - 1]
                or "土城區" == mailingRegion[self.count - 1]
                or "中和區" == mailingRegion[self.count - 1]
                or "永和區" == mailingRegion[self.count - 1]
                or "汐止區" == mailingRegion[self.count - 1]
                or "板橋區" == mailingRegion[self.count - 1]
                or "林口區" == mailingRegion[self.count - 1]
                or "新店區" == mailingRegion[self.count - 1]
                or "新莊區" == mailingRegion[self.count - 1]
                or "蘆洲區" == mailingRegion[self.count - 1]
                or "五股區" == mailingRegion[self.count - 1]
                or "樹林區" == mailingRegion[self.count - 1]
                or "淡水區" == mailingRegion[self.count - 1]
                or "重莊特區" == mailingRegion[self.count - 1]
                and mailingAddress.find("新北市") != -1
            ):
                self.mailingAddressList.append(mailingAddress + "號")
            elif (
                "士林區" == mailingRegion[self.count - 1]
                or "大同區" == mailingRegion[self.count - 1]
                or "大安區" == mailingRegion[self.count - 1]
                or "中山一區" == mailingRegion[self.count - 1]
                or "中山二區" == mailingRegion[self.count - 1]
                or "中山區" == mailingRegion[self.count - 1]
                or "中正區" == mailingRegion[self.count - 1]
                or "內湖特區" == mailingRegion[self.count - 1]
                or "內湖區" == mailingRegion[self.count - 1]
                or "文山區" == mailingRegion[self.count - 1]
                or "北投區" == mailingRegion[self.count - 1]
                or "松山區" == mailingRegion[self.count - 1]
                or "信義區" == mailingRegion[self.count - 1]
                or "南港區" == mailingRegion[self.count - 1]
                or "萬華區" == mailingRegion[self.count - 1]
                or "天母區" == mailingRegion[self.count - 1]
                or "大直區" == mailingRegion[self.count - 1]
                or "舊莊區" == mailingRegion[self.count - 1]
                and mailingAddress.find("台北市") == -1
            ):
                # print("台北市" + mailingAddress)
                self.mailingAddressList.append("台北市" + mailingAddress + "號")
            elif (
                "士林區" == mailingRegion[self.count - 1]
                or "大同區" == mailingRegion[self.count - 1]
                or "大安區" == mailingRegion[self.count - 1]
                or "中山一區" == mailingRegion[self.count - 1]
                or "中山二區" == mailingRegion[self.count - 1]
                or "中山區" == mailingRegion[self.count - 1]
                or "中正區" == mailingRegion[self.count - 1]
                or "內湖特區" == mailingRegion[self.count - 1]
                or "內湖區" == mailingRegion[self.count - 1]
                or "文山區" == mailingRegion[self.count - 1]
                or "北投區" == mailingRegion[self.count - 1]
                or "松山區" == mailingRegion[self.count - 1]
                or "信義區" == mailingRegion[self.count - 1]
                or "南港區" == mailingRegion[self.count - 1]
                or "萬華區" == mailingRegion[self.count - 1]
                or "天母區" == mailingRegion[self.count - 1]
                or "大直區" == mailingRegion[self.count - 1]
                or "舊莊區" == mailingRegion[self.count - 1]
                and mailingAddress.find("台北市") != -1
            ):
                self.mailingAddressList.append(mailingAddress + "號")
            else:
                self.mailingAddressList.append(mailingAddress)
        else:
            if mailingAddress.find("新北市") != -1:
                # print(mailingAddress)
                self.mailingAddressList.append(mailingAddress)
            elif mailingAddress.find("台北市") != -1:
                # print(mailingAddress)
                self.mailingAddressList.append(mailingAddress)
            elif mailingAddress.find("臺北市") != -1:
                # print(mailingAddress)
                self.mailingAddressList.append(mailingAddress)
            elif (
                "三重區" == mailingRegion[self.count - 1]
                or "土城區" == mailingRegion[self.count - 1]
                or "中和區" == mailingRegion[self.count - 1]
                or "永和區" == mailingRegion[self.count - 1]
                or "汐止區" == mailingRegion[self.count - 1]
                or "板橋區" == mailingRegion[self.count - 1]
                or "林口區" == mailingRegion[self.count - 1]
                or "新店區" == mailingRegion[self.count - 1]
                or "新莊區" == mailingRegion[self.count - 1]
                or "蘆洲區" == mailingRegion[self.count - 1]
                or "五股區" == mailingRegion[self.count - 1]
                or "樹林區" == mailingRegion[self.count - 1]
                or "淡水區" == mailingRegion[self.count - 1]
                or "重莊特區" == mailingRegion[self.count - 1]
                and mailingAddress.find("新北市") == -1
            ):
                # print("新北市" + mailingAddress)
                self.mailingAddressList.append("新北市" + mailingAddress)
            elif (
                "三重區" == mailingRegion[self.count - 1]
                or "土城區" == mailingRegion[self.count - 1]
                or "中和區" == mailingRegion[self.count - 1]
                or "永和區" == mailingRegion[self.count - 1]
                or "汐止區" == mailingRegion[self.count - 1]
                or "板橋區" == mailingRegion[self.count - 1]
                or "林口區" == mailingRegion[self.count - 1]
                or "新店區" == mailingRegion[self.count - 1]
                or "新莊區" == mailingRegion[self.count - 1]
                or "蘆洲區" == mailingRegion[self.count - 1]
                or "五股區" == mailingRegion[self.count - 1]
                or "樹林區" == mailingRegion[self.count - 1]
                or "淡水區" == mailingRegion[self.count - 1]
                or "重莊特區" == mailingRegion[self.count - 1]
                and mailingAddress.find("新北市") != -1
            ):
                self.mailingAddressList.append(mailingAddress)
            elif (
                "士林區" == mailingRegion[self.count - 1]
                or "大同區" == mailingRegion[self.count - 1]
                or "大安區" == mailingRegion[self.count - 1]
                or "中山一區" == mailingRegion[self.count - 1]
                or "中山二區" == mailingRegion[self.count - 1]
                or "中山區" == mailingRegion[self.count - 1]
                or "中正區" == mailingRegion[self.count - 1]
                or "內湖特區" == mailingRegion[self.count - 1]
                or "內湖區" == mailingRegion[self.count - 1]
                or "文山區" == mailingRegion[self.count - 1]
                or "北投區" == mailingRegion[self.count - 1]
                or "松山區" == mailingRegion[self.count - 1]
                or "信義區" == mailingRegion[self.count - 1]
                or "南港區" == mailingRegion[self.count - 1]
                or "萬華區" == mailingRegion[self.count - 1]
                or "天母區" == mailingRegion[self.count - 1]
                or "大直區" == mailingRegion[self.count - 1]
                or "舊莊區" == mailingRegion[self.count - 1]
                and mailingAddress.find("台北市") == -1
            ):
                # print("台北市" + mailingAddress)
                self.mailingAddressList.append("台北市" + mailingAddress)
            elif (
                "士林區" == mailingRegion[self.count - 1]
                or "大同區" == mailingRegion[self.count - 1]
                or "大安區" == mailingRegion[self.count - 1]
                or "中山一區" == mailingRegion[self.count - 1]
                or "中山二區" == mailingRegion[self.count - 1]
                or "中山區" == mailingRegion[self.count - 1]
                or "中正區" == mailingRegion[self.count - 1]
                or "內湖特區" == mailingRegion[self.count - 1]
                or "內湖區" == mailingRegion[self.count - 1]
                or "文山區" == mailingRegion[self.count - 1]
                or "北投區" == mailingRegion[self.count - 1]
                or "松山區" == mailingRegion[self.count - 1]
                or "信義區" == mailingRegion[self.count - 1]
                or "南港區" == mailingRegion[self.count - 1]
                or "萬華區" == mailingRegion[self.count - 1]
                or "天母區" == mailingRegion[self.count - 1]
                or "大直區" == mailingRegion[self.count - 1]
                or "舊莊區" == mailingRegion[self.count - 1]
                and mailingAddress.find("台北市") != -1
            ):
                self.mailingAddressList.append(mailingAddress)
            else:
                self.mailingAddressList.append(mailingAddress)

    def recursive(self, mailingAddressUncomplete, mailingRegion):
        for mailingAddress in mailingAddressUncomplete:
            self.count += 1
            # print(self.count)
            # print(mailingRegion[self.count-1])
            index = mailingAddress.find("一")
            index_2 = mailingAddress.find("二")
            index_3 = mailingAddress.find("三")
            index_4 = mailingAddress.find("四")
            index_5 = mailingAddress.find("五")
            index_6 = mailingAddress.find("六")
            index_7 = mailingAddress.find("七")
            index_8 = mailingAddress.find("八")
            index_9 = mailingAddress.find("九")

            if index != -1:  # 地址已經有「一」
                if mailingAddress.find("段") != -1:  # 地址已經有寫段
                    self.regionAdded(mailingAddress, mailingRegion)
                elif mailingAddress.find("路") != -1:  # 地址已經有寫路
                    mailingAddress = (
                        mailingAddress[: index + 1] + "段" + mailingAddress[index + 1 :]
                    )
                    self.regionAdded(mailingAddress, mailingRegion)
                elif index == 0:  # 例外處理:一在地址最前面
                    self.regionAdded(mailingAddress, mailingRegion)
                else:  # 地址沒有寫段跟路
                    mailingAddress = (
                        mailingAddress[:index]
                        + "路"
                        + mailingAddress[index : index + 1]
                        + "段"
                        + mailingAddress[index + 1 :]
                    )
                    self.regionAdded(mailingAddress, mailingRegion)

            elif index_2 != -1:  # 地址已經有「二」
                if mailingAddress.find("段") != -1:  # 地址已經有寫段
                    self.regionAdded(mailingAddress, mailingRegion)
                elif mailingAddress.find("路") != -1:  # 地址已經有寫路
                    mailingAddress = (
                        mailingAddress[: index_2 + 1]
                        + "段"
                        + mailingAddress[index_2 + 1 :]
                    )
                    self.regionAdded(mailingAddress, mailingRegion)
                elif index_2 == 0:  # 例外處理:二在地址最前面
                    self.regionAdded(mailingAddress, mailingRegion)
                else:
                    mailingAddress = (
                        mailingAddress[:index_2]
                        + "路"
                        + mailingAddress[index_2 : index_2 + 1]
                        + "段"
                        + mailingAddress[index_2 + 1 :]
                    )
                    self.regionAdded(mailingAddress, mailingRegion)

            elif index_3 != -1:  # 地址已經有「三」
                if mailingAddress.find("段") != -1:  # 地址已經有寫段
                    self.regionAdded(mailingAddress, mailingRegion)
                elif mailingAddress.find("路") != -1:  # 地址已經有寫路
                    mailingAddress = (
                        mailingAddress[: index_3 + 1]
                        + "段"
                        + mailingAddress[index_3 + 1 :]
                    )
                    self.regionAdded(mailingAddress, mailingRegion)
                elif index_3 == 0:  # 例外處理:三在地址最前面
                    self.regionAdded(mailingAddress, mailingRegion)
                else:
                    final_string = (
                        mailingAddress[:index_3]
                        + "路"
                        + mailingAddress[index_3 : index_3 + 1]
                        + "段"
                        + mailingAddress[index_3 + 1 :]
                    )
                    self.regionAdded(mailingAddress, mailingRegion)

            elif index_4 != -1:  # 地址已經有「四」
                if mailingAddress.find("段") != -1:  # 地址已經有寫段
                    self.regionAdded(mailingAddress, mailingRegion)
                elif mailingAddress.find("路") != -1:  # 地址已經有寫路
                    mailingAddress = (
                        mailingAddress[: index_4 + 1]
                        + "段"
                        + mailingAddress[index_4 + 1 :]
                    )
                    self.regionAdded(mailingAddress, mailingRegion)
                elif index_4 == 0:  # 例外處理:四在地址最前面
                    self.regionAdded(mailingAddress, mailingRegion)
                else:
                    mailingAddress = (
                        mailingAddress[:index_4]
                        + "路"
                        + mailingAddress[index_4 : index_4 + 1]
                        + "段"
                        + mailingAddress[index_4 + 1 :]
                    )
                    self.regionAdded(mailingAddress, mailingRegion)

            elif index_5 != -1:  # 地址已經有「五」
                if mailingAddress.find("段") != -1:  # 地址已經有寫段
                    self.regionAdded(mailingAddress, mailingRegion)
                elif mailingAddress.find("路") != -1:  # 地址已經有寫路
                    mailingAddress = (
                        mailingAddress[: index_5 + 1]
                        + "段"
                        + mailingAddress[index_5 + 1 :]
                    )
                    self.regionAdded(mailingAddress, mailingRegion)
                elif index_5 == 0:  # 例外處理:五在地址最前面
                    self.regionAdded(mailingAddress, mailingRegion)
                else:
                    mailingAddress = (
                        mailingAddress[:index_5]
                        + "路"
                        + mailingAddress[index_5 : index_5 + 1]
                        + "段"
                        + mailingAddress[index_5 + 1 :]
                    )
                    self.regionAdded(mailingAddress, mailingRegion)

            elif index_6 != -1:  # 地址已經有「六」
                if mailingAddress.find("段") != -1:  # 地址已經有寫段
                    self.regionAdded(mailingAddress, mailingRegion)
                elif mailingAddress.find("路") != -1:  # 地址已經有寫路
                    mailingAddress = (
                        mailingAddress[: index_6 + 1]
                        + "段"
                        + mailingAddress[index_6 + 1 :]
                    )
                    self.regionAdded(mailingAddress, mailingRegion)
                elif index_6 == 0:  # 例外處理:六在地址最前面
                    self.regionAdded(mailingAddress, mailingRegion)
                else:
                    mailingAddress = (
                        mailingAddress[:index_6]
                        + "路"
                        + mailingAddress[index_6 : index_6 + 1]
                        + "段"
                        + mailingAddress[index_6 + 1 :]
                    )
                    self.regionAdded(mailingAddress, mailingRegion)

            elif index_7 != -1:  # 地址已經有「七」
                if mailingAddress.find("段") != -1:  # 地址已經有寫段
                    self.regionAdded(mailingAddress, mailingRegion)
                elif mailingAddress.find("路") != -1:  # 地址已經有寫路
                    mailingAddress = (
                        mailingAddress[: index_7 + 1]
                        + "段"
                        + mailingAddress[index_7 + 1 :]
                    )
                    self.regionAdded(mailingAddress, mailingRegion)
                elif index_7 == 0:  # 例外處理:七在地址最前面
                    self.regionAdded(mailingAddress, mailingRegion)
                else:  # 地址沒有寫路跟段
                    mailingAddress = (
                        mailingAddress[:index_7]
                        + "路"
                        + mailingAddress[index_7 : index_7 + 1]
                        + "段"
                        + mailingAddress[index_7 + 1 :]
                    )
                    self.regionAdded(mailingAddress, mailingRegion)

            elif index_8 != -1:  # 地址已經有「八」
                if mailingAddress.find("段") != -1:  # 地址已經有寫段
                    self.regionAdded(mailingAddress, mailingRegion)
                elif mailingAddress.find("路") != -1:  # 地址已經有寫路
                    mailingAddress = (
                        mailingAddress[: index_8 + 1]
                        + "段"
                        + mailingAddress[index_8 + 1 :]
                    )
                    self.regionAdded(mailingAddress, mailingRegion)
                elif index_8 == 0:  # 例外處理:八在地址最前面
                    self.regionAdded(mailingAddress, mailingRegion)
                else:
                    mailingAddress = (
                        mailingAddress[:index_8]
                        + "路"
                        + mailingAddress[index_8 : index_8 + 1]
                        + "段"
                        + mailingAddress[index_8 + 1 :]
                    )
                    self.regionAdded(mailingAddress, mailingRegion)

            elif index_9 != -1:  # 地址已經有「九」
                if mailingAddress.find("段") != -1:  # 地址已經有寫段
                    self.regionAdded(mailingAddress, mailingRegion)
                elif mailingAddress.find("路") != -1:  # 地址已經有寫路
                    mailingAddress = (
                        mailingAddress[: index_9 + 1]
                        + "段"
                        + mailingAddress[index_9 + 1 :]
                    )
                    self.regionAdded(mailingAddress, mailingRegion)
                elif index_9 == 0:  # 例外處理:九在地址最前面
                    self.regionAdded(mailingAddress, mailingRegion)

                else:
                    mailingAddress = (
                        mailingAddress[:index_9]
                        + "路"
                        + mailingAddress[index_9 : index_9 + 1]
                        + "段"
                        + mailingAddress[index_9 + 1 :]
                    )
                    self.regionAdded(mailingAddress, mailingRegion)
            else:
                self.regionAdded(mailingAddress, mailingRegion)


if __name__ == "__main__":
    # app.run(debug=False)
    file_path = "order_data_repair.csv"
    df = pd.read_csv(file_path)
    test_obj = address_preprocess()
    order_data = test_obj.get_complete_address(df)
    test_obj.insert_all(order_data)
